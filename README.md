# Gather

On the spot hangouts.

## Getting Started
Gather makes it easy to create and organize events with your friends spontaneously. By displaying the important event details (time, date, location, who's attending) in a user-friendly way, we save time for you and the host. With a built-in convenient chatroom centered around the event themselves, we eliminate the countless group-chats you used to have for each variation of the friend circle.