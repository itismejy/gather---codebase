import 'package:flutter_local_notifications/flutter_local_notifications.dart';

FlutterLocalNotificationsPlugin _plugin = new FlutterLocalNotificationsPlugin();

void initializeNotificationPlugin() {
// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  var initializationSettingsAndroid =
      new AndroidInitializationSettings('app_icon');
  var initializationSettingsIOS = IOSInitializationSettings();
  var initializationSettings = InitializationSettings(
      initializationSettingsAndroid, initializationSettingsIOS);
  _plugin.initialize(initializationSettings);
}

Future<List<PendingNotificationRequest>> pending() async {
  var pendingNotificationRequests = await _plugin.pendingNotificationRequests();
  return pendingNotificationRequests;
}

Future<void> notify(DateTime time, String title, String body, int id) async {
  var pendingNotificationRequests = await _plugin.pendingNotificationRequests();
  List<int> notifRequests = [];
  for (int i = 0; i < pendingNotificationRequests.length; i++) {
    notifRequests.add(pendingNotificationRequests[i].id);
  }
  if (notifRequests.contains(id)) {
    print("contains");
    await _plugin.cancel(id);
  }
    if (time.isBefore(DateTime.now())) {
      return;
    }    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'a', 'EventNotifications', 'Notifications for events',
        importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    _plugin.schedule(
      id,
      title,
      body,
      time,
      platformChannelSpecifics,
    );
  
  return null;
}
