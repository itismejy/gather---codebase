import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'colors.dart' as app_colors;
import 'components.dart';
import 'text_styles.dart' as T;
import 'classes.dart';
import 'firebase_backend.dart' as fbe;

void main() => runApp(AddFriends());

class AddFriends extends StatelessWidget {
  Widget build(BuildContext context) {
    return AFP();
  }
}

class AFP extends StatefulWidget {
  final List<User> friendrequest;
  AFP({Key key, this.friendrequest}) : super(key: key);

  State<StatefulWidget> createState() {
    return AddFriendsPage(friendrequest: [], friendadd: []);
  }
}

class AddFriendsPage extends State<AFP> {
  String friendSearch = "";
  List<User> friendrequest;
  List<User> friendadd;
  Future<List<User>> _add;
  Future<List<User>> _request;
  AddFriendsPage({this.friendrequest, this.friendadd});
  List<User> queryName(List<User> friends) =>
      friends.where((name) => name.userName.startsWith(friendSearch)).toList();

  @override
  initState() {
    super.initState();
    _add = fbe.getNonFriends();
    _request = fbe.getFriendRequests();
  }

  void toggleFriendClicked(User user) {
    if (user.clicked == true) {
      user.clicked = false;
    } else {
      user.clicked = true;
    }
        _add = fbe.getNonFriends();
  }

  Widget friendListIcon(List<User> friends, User friendProf, bool request,
      {bool check = false}) {
    return Container(
      alignment: Alignment(100 * wr, 0),
      width: 30.0 * wr,
      height: 30.0 * wr,
      decoration: BoxDecoration(
        shape: (request ? BoxShape.circle : BoxShape.rectangle),
        // color: app_colors.SWATCH_B,
      ),
      child: IconButton(
          padding: EdgeInsets.only(bottom: 40 * hr),
          icon: Icon(
              request
                  ? (check ? Icons.check_circle : Icons.cancel)
                  : Icons.add_box,
              color: request
                  ? (check ? app_colors.SWATCH_A : Colors.red)
                  : app_colors.SWATCH_A,
              size: 30 * wr),
          onPressed: () async {
            if (request) {
              FutureBuilder<void>(
                  future: fbe.respondFriendRequest(friendProf.userID, check),
                  builder:
                      (BuildContext context, AsyncSnapshot<void> snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        return Text("Failed to retrieve feed");
                      case ConnectionState.active:
                      case ConnectionState.waiting:
                        return SpinKitWanderingCubes(color: Colors.green);
                      case ConnectionState.done:
                        if (snapshot.hasError) {
                          return Text("Error: ${snapshot.error}");
                        }
                    }
                    return null;
                  });
              Navigator.of(context).pop();
              Navigator.of(context).pop();
              Navigator.pushNamed(context, '/profile');
              Navigator.pushNamed(context, '/add_friends');
              friendadd = await fbe.getNonFriends();
              friendrequest = await fbe.getFriendRequests();
              // setState(() {});
              // Navigator.pushNamed(context, '/add_friends');
              // if (check == true) {
              //   _showToast(context,
              //       friendProf.userName + "'s friend request was accepted");
              // } else {
              //   _showToast(context,
              //       friendProf.userName + "'s friend request was rejected");
              // }
            } else {
              FutureBuilder<void>(
                  future: fbe.sendFriendRequests(friendProf.userID),
                  builder:
                      (BuildContext context, AsyncSnapshot<void> snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        return Text("Failed to retrieve feed");
                      case ConnectionState.active:
                      case ConnectionState.waiting:
                        return SpinKitWanderingCubes(color: Colors.green);
                      case ConnectionState.done:
                        if (snapshot.hasError) {
                          return Text("Error: ${snapshot.error}");
                        }
                    }
                    return null;
                  });
              // Navigator.of(context).pop();
              // Navigator.of(context).pop();
              // Navigator.pushNamed(context, '/profile');
              // Navigator.pushNamed(context, '/add_friends');

              // friendadd = await fbe.getNonFriends();
              // friendrequest = await fbe.getFriendRequests();
              _showToast(
                  context, friendProf.userName + " was sent a friend request");
            }
            friends.remove(friendProf);
            toggleFriendClicked(friendProf);
            setState(() {});
          }),
    );
  }

  void _showToast(BuildContext context, String text) {
    final scaffold = _scaffoldKey.currentState;
    scaffold.showSnackBar(
      SnackBar(
        content: Text(
          text,
          style: TextStyle(
              color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12),
        ),
        backgroundColor: app_colors.SWATCH_B,
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  Widget friendRequestIcon(List<User> friends, User friendProf) {
    return Row(children: <Widget>[
      friendListIcon(friends, friendProf, true, check: true),
      SizedBox(width: 10 * wr), // Invisible box between accept / reject icons
      friendListIcon(friends, friendProf, true)
    ]);
  }

  Widget friendAddIcon(List<User> friends, User friendProf) {
    return friendListIcon(friends, friendProf, false);
  }

  Widget renderFriendList1(List<User> friends, bool request) {
    return FutureBuilder<List<User>>(
        future: fbe.getMutualFriends2(friends),
        builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text("Failed to retrieve feed");
            case ConnectionState.active:
            case ConnectionState.waiting:
              return SpinKitWanderingCubes(color: Colors.green);
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text("Error: ${snapshot.error}");
              }
              return renderFriendList2(snapshot.data, request);
          }
          return null;
        });
  }

  Widget renderFriendList2(List<User> friends, bool request) {
    return FutureBuilder<List<int>>(
        future: fbe.getMutualFriends3(friends),
        builder: (BuildContext context, AsyncSnapshot<List<int>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text("Failed to retrieve feed");
            case ConnectionState.active:
            case ConnectionState.waiting:
              return SpinKitWanderingCubes(color: Colors.green);
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text("Error: ${snapshot.error}");
              }
              return renderFriendList3(friends, request, snapshot.data);
          }
          return null;
        });
  }

  Widget renderFriendList3(
      List<User> friends, bool request, List<int> sharedInts) {
    return ListView.builder(
        itemCount: friends.length,
        itemBuilder: (BuildContext ctxt, int index) {
          //sort here
          User friendProf = friends[index];
          return FutureBuilder<int>(
              future: fbe.getMutualFriends(friendProf.userID, fbu.uid),
              builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return Text("Failed to retrieve feed");
                  case ConnectionState.active:
                  case ConnectionState.waiting:
                    return Container();
                  // return SpinKitWanderingCubes(color: Colors.green);
                  case ConnectionState.done:
                    if (snapshot.hasError) {
                      return Text("Error: ${snapshot.error}");
                    }
                    return renderSingleFriendList(friends, friendProf, request,
                        snapshot.data, sharedInts, index);
                }
                return null;
              });
        });
  }

  Widget renderSingleFriendList(List<User> friends, User friendProf,
      bool request, int shared, List<int> sharedInts, int index) {
    return Center(
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      SizedBox(width: 10 * wr, height: 50 * hr),
      Container(
          width: 40.0 * wr,
          height: 40.0 * wr,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.fill, image: friendProf.userImage))),
      SizedBox(width: 10 * wr, height: 50 * hr),
      Expanded(
        child: Container(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(friendProf.userName,
                    textAlign: TextAlign.left,
                    textDirection: TextDirection.ltr,
                    style: T.small),
                Text(
                  (shared <= 1)
                      ? (shared == 0) ? "No Mutual Friends" : "1 Mutual Friend"
                      : shared.toString() + " Mutual Friends",
                  textAlign: TextAlign.left,
                  textDirection: TextDirection.ltr,
                  style: T.extraSmall,
                ),
              ]),
        ),
      ),
      // SizedBox(width: (request ? 50:60)),
      Center(
          child: (request
              ? friendRequestIcon(friends, friendProf)
              : friendAddIcon(friends, friendProf))),
      //  Padding(
      //      padding: EdgeInsets.only(top: 10),
      //      child:
      //        (request ? friendReqestIcon(friends, friendProf) : friendAddIcon(friends, friendProf))
      //      ),
      SizedBox(width: 10 * wr, height: 50 * hr)
    ]));
  }

  final searchKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: app_colors.SWATCH_B,
        appBar: topBar(true, context, false),
        key: _scaffoldKey,
        body: Stack(children: <Widget>[
          Column(children: [
            Padding(
                padding: EdgeInsets.only(top: 20 * hr),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Form(
                              key: searchKey,
                              child: Expanded(
                                  child: TextField(
                                      onChanged: (newString) {
                                        setState(() {
                                          friendSearch = newString;
                                        });
                                      },
                                      decoration: InputDecoration(
                                        hintText: 'Search',
                                        counterText: "",
                                        border: InputBorder.none,
                                        contentPadding: EdgeInsets.fromLTRB(
                                            10.0 * wr,
                                            10.0 * hr,
                                            20.0 * wr,
                                            10.0 * hr),
                                      ))))
                        ],
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(25.0)),
                          color: Colors.white),
                      height: 30 * hr,
                      width: getWidth() - 20 * wr,
                    ),
                  ],
                ))
          ]),
          Padding(
              padding: EdgeInsets.only(top: (100 + (getHeight() / 3) * hr)),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Column(children: <Widget>[
                        SizedBox(height: 10 * hr),
                        Text('- Friend Requests -', style: T.medium),
                        Expanded(
                            child: FutureBuilder<List<User>>(
                                future: _request,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<User>> snapshot) {
                                  switch (snapshot.connectionState) {
                                    case ConnectionState.none:
                                      return Text("Failed to retrieve feed");
                                    case ConnectionState.active:
                                    case ConnectionState.waiting:
                                      return SpinKitWanderingCubes(
                                          color: Colors.green);
                                    case ConnectionState.done:
                                      if (snapshot.hasError) {
                                        return Text("Error: ${snapshot.error}");
                                      }
                                      return renderFriendList1(
                                          snapshot.data, true);
                                  }
                                  return null;
                                })),
                      ]),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          color: Colors.white),
                      height: (pheight * hr / 3),
                      width: (pwidth - 20) * wr,
                    )
                  ])),
          Padding(
              padding: EdgeInsets.only(top: 70 * hr),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Column(children: <Widget>[
                        SizedBox(height: 10 * hr),
                        Text('- Add Friends -', style: T.medium),
                        Expanded(
                            child: FutureBuilder<List<User>>(
                                future: _add,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<User>> snapshot) {
                                  switch (snapshot.connectionState) {
                                    case ConnectionState.none:
                                      return Text("Failed to retrieve feed");
                                    case ConnectionState.active:
                                    case ConnectionState.waiting:
                                      return SpinKitWanderingCubes(
                                          color: Colors.green);
                                    case ConnectionState.done:
                                      if (snapshot.hasError) {
                                        return Text("Error: ${snapshot.error}");
                                      }
                                      return renderFriendList1(
                                          snapshot.data, false);
                                  }
                                  return null;
                                })),
                      ]),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          color: Colors.white),
                      height: (pheight * hr / 3),
                      width: (pwidth - 20) * wr,
                    )
                  ])),
        ]));
  }
}
