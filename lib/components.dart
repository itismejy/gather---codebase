import 'package:flutter/material.dart';
import 'colors.dart' as app_colors;
import 'text_styles.dart' as text_styles;
import "strings.dart" as strings;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

// This function is the task  bar at the top of the screen
AppBar topBar(bool leftIcon, BuildContext context, bool settings) {
  var appBar = AppBar(
    backgroundColor: app_colors.HEADER,
    title: Text(
      strings.TITLE,
      style: text_styles.withColor(app_colors.TEXT_PRIMARY, text_styles.header),
    ),
    centerTitle: true,
    leading: IconButton(
        icon: Icon(leftIcon ? Icons.arrow_back : Icons.person,
            color: app_colors.ICON),
        onPressed: () {
          leftIcon
              ? Navigator.of(context).pop()
              : Navigator.pushNamed(context, '/profile');
        }),
    actions: <Widget>[
      Padding(
          padding: const EdgeInsets.only(right: 0),
          child: (!leftIcon)
              ? IconButton(
                  icon: Icon(
                    Icons.date_range,
                    color: app_colors.ICON,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/pastscreen');
                  })
              : Container()),
      Padding(
          padding: const EdgeInsets.only(right: 8),
          child: (!settings)
              ? IconButton(
                  icon: Icon(
                    Icons.settings,
                    color: app_colors.ICON,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/settings');
                  })
              : Container())
    ],
  );
  return appBar;
}

// Functions to set and get the size variables of the screen
double width;
double height;
Size size;
double wr; //Width Ratio
double hr; //Height Ratio
double sr; //Size Ratio

const double pwidth = 360;
const double pheight = 640;

void setWidth(double w) {
  width = w;
}

void setHeight(double h) {
  height = h;
}

void setSize(Size s) {
  size = s;
}

void setwr(double w) {
  wr = w;
}

void sethr(double h) {
  hr = h;
}

void setsr(double s) {
  sr = s;
}

double getWidth() {
  return width;
}

double getHeight() {
  return height;
}

Size getSize() {
  return size;
}

double getwr() {
  return wr;
}

double gethr() {
  return hr;
}

double getsr() {
  return sr;
}

class CustomPicker extends TimePickerModel {
  String digits(int value, int length) {
    return '$value'.padLeft(length, "0");
  }

  CustomPicker({DateTime currentTime, LocaleType locale})
      : super(locale: locale) {
    this.currentTime = currentTime ?? DateTime.now();
    if (this.currentTime.hour == 0) {
      this.setLeftIndex(12);
      this.setRightIndex(0);
    } else if (this.currentTime.hour > 0 && this.currentTime.hour < 12) {
      this.setLeftIndex(this.currentTime.hour);
      this.setRightIndex(0);
    } else if (this.currentTime.hour == 12) {
      this.setLeftIndex(this.currentTime.hour);
      this.setRightIndex(1);
    } else {
      this.setLeftIndex(this.currentTime.hour - 12);
      this.setRightIndex(1);
    }
    this.setMiddleIndex(this.currentTime.minute);
  }
  @override
  String leftStringAtIndex(int index) {
    if (index > 0 && index <= 12) {
      return this.digits(index, 2);
    } else {
      return null;
    }
  }

  @override
  String middleStringAtIndex(int index) {
    if (index >= 0 && index < 60) {
      return this.digits(index, 2);
    } else {
      return null;
    }
  }

  @override
  String rightStringAtIndex(int index) {
    if (index >= 0 && index < 2) {
      if (index == 0) {
        return "AM";
      } else {
        return "PM";
      }
    } else {
      return null;
    }
  }

  @override
  String leftDivider() {
    return "|";
  }

  @override
  String rightDivider() {
    return "|";
  }

  @override
  List<int> layoutProportions() {
    return [1, 1, 1];
  }

  @override
  DateTime finalTime() {
    int hour = this.currentLeftIndex();
    if (this.currentRightIndex() == 0 && this.currentLeftIndex() == 12) {
      hour -= 12;
    } else if (this.currentRightIndex() == 1 && this.currentLeftIndex() < 12) {
      hour += 12;
    }
    return currentTime.isUtc
        ? DateTime.utc(
            currentTime.year,
            currentTime.month,
            currentTime.day,
            hour,
            this.currentMiddleIndex(),
          )
        : DateTime(
            currentTime.year,
            currentTime.month,
            currentTime.day,
            hour,
            this.currentMiddleIndex(),
          );
  }
}

DateTime getDateTimeT(DateTime date, DateTime time) {
  return new DateTime(date.year, date.month, date.day, time.hour, time.minute);
}

DateTime getDateTime(
    bool ifStart, DateTime date, String rawStart, String rawEnd) {
  List<String> start = rawStart.split(":");
  List<String> end = rawEnd.split(":");
  if (ifStart == true) {
    return new DateTime(date.year, date.month, date.day, int.parse(start[0]),
        int.parse(start[1]));
  } else {
    return new DateTime(
        date.year, date.month, date.day, int.parse(end[0]), int.parse(end[1]));
  }
}

List<String> getDayAndTime(DateTime start, DateTime end) {
  List<String> list = [];
  list.add(start.month.toString() +
      "/" +
      start.day.toString() +
      "/" +
      start.year.toString());
  String startMin = start.minute.toString();
  String endMin = end.minute.toString();
  String startAMorPM = "AM";
  String endAMorPM = "AM";
  String startHour = start.hour.toString();
  String endHour = end.hour.toString();
  if (start.hour == 0) {
    startHour = "12";
  }
  if (end.hour == 0) {
    endHour = "12";
  }
  if (start.hour > 12) {
    startHour = (start.hour - 12).toString();
    startAMorPM = "PM";
  }
  if (end.hour > 12) {
    endHour = (end.hour - 12).toString();
    endAMorPM = "PM";
  }
  if (start.minute < 10) {
    startMin = "0" + startMin;
  }
  if (end.minute < 10) {
    endMin = "0" + endMin;
  }
  list.add(startHour +
      ":" +
      startMin +
      " " +
      startAMorPM +
      " to " +
      endHour +
      ":" +
      endMin +
      " " +
      endAMorPM);
  return list;
}

String parseDateTime(DateTime time) {
  String minute = "";
  String hour = "";
  String amOrPm = "AM";
  if (time.minute < 10) {
    minute = '0' + time.minute.toString();
  } else {
    minute = time.minute.toString();
  }
  if (time.hour == 0) {
    hour = '12';
  } else if (time.hour < 12 && time.hour > 0) {
    hour = time.hour.toString();
  } else if (time.hour == 12) {
    hour = time.hour.toString();
    amOrPm = "PM";
  } else {
    amOrPm = "PM";

    hour = (time.hour - 12).toString();
  }
  return hour + ' : ' + minute + ' ' + amOrPm;
}

List<String> editInfoInitDayTime(DateTime start, DateTime end) {
  List<String> list = [];
  list.add(start.month.toString() +
      "/" +
      start.day.toString() +
      "/" +
      start.year.toString());
  list.add(start.hour.toString() + ":" + start.minute.toString());
  list.add(end.hour.toString() + ":" + end.minute.toString());
  return list;
}
