import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:grouper/text_styles.dart';
import 'colors.dart' as app_colors;
import 'colors.dart';
import 'strings.dart';
import 'text_styles.dart' as T;
import 'classes.dart';
import 'firebase_backend.dart' as fbe;
import 'components.dart';

final storage = FlutterSecureStorage();

class SignIn extends StatefulWidget {
  State<StatefulWidget> createState() {
    return SignInPage();
  }
}

class SignInPage extends State<SignIn> {
  // Vars for the typebars
  TextEditingController userController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  bool isInvisible = true;

  // Allows for state change
  @override
  void initState() {
    super.initState();
    loadSavedLogin();
  }

  //Method to change whether password is visible or not
  void _toggle() {
    setState(() {
      isInvisible = !isInvisible;
    });
  }

  // Widget for the bar to store user information
  Widget typeBar(IconData icon, String text, double pad,
      TextEditingController controller, bool isPassword) {
    return Padding(
        padding: EdgeInsets.only(top: pad), // Vertical Padding
        child: Container(
          // Container that holds the typebar
          height: 40 * hr,
          width: 270 * wr,
          child: Row(
              // Row of typebar elements
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 5 * wr), // Horizontal Spacing
                Icon(icon, color: app_colors.TEXT_APPNAME), // Specified Icon
                SizedBox(width: 5 * wr), // Horizontal Spacing
                Expanded(
                  // Container for text
                  child: TextField(
                      // Widget to enter text
                      controller: controller,
                      obscureText: (isPassword &&
                          isInvisible), // Check to see if the text should be hidden
                      decoration: InputDecoration.collapsed(
                          hintText: text) // Display the prompt
                      ),
                ),
                (isPassword)
                    ? SizedBox(
                        // If it is a password typebar display the red eye icon
                        width: 40 * wr,
                        child: IconButton(
                            padding: EdgeInsets.zero,
                            icon: Icon(Icons.remove_red_eye,
                                color: app_colors.TEXT_APPNAME),
                            onPressed: () {
                              _toggle();
                            } // Call the toggle to change the password display
                            ))
                    : SizedBox(
                        width: 10 *
                            wr) // If it is not a password typebar just add spacing
              ]),
          decoration: BoxDecoration(
              // Make the typebar be white with rounded corners
              borderRadius: BorderRadius.all(Radius.circular(10.0 * sr)),
              color: Colors.white),
        ));
  }

  // Load in old user data if it exists
  void loadSavedLogin() async {
    var oldUsername = await storage.read(key: "username");
    var oldPassword = await storage.read(key: "password");
    if (oldUsername != null) {
      userController.text = oldUsername;
    }
    if (oldPassword != null) {
      passController.text = oldPassword;
    }
  }

  @override
  Widget build(BuildContext context) {
    // SET THE SIZE VARS
    Size size = MediaQuery.of(context).size;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    setSize(size);
    setWidth(width);
    setHeight(height);
    setwr(width / pwidth);
    sethr(height / pheight);
    //sr should just be average of the wr and hr
    setsr((wr + hr) / 2);
    // DONE SETTING THE SIZE VARS

    return new WillPopScope(
        // User cant press back button on this page
        onWillPop: () async => false,
        child: Scaffold(body: Builder(builder: (context) {
          return SingleChildScrollView(
              // Page is scrollable - for when the keyboard pops up
              child: Container(
                  width: width,
                  height: height,
                  // Background Gradient
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            app_colors.GRADIENT_A,
                            app_colors.GRADIENT_B
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp)),
                  // Main column for layout
                  child: Column(children: <Widget>[
                    // Vertical Spacing
                    SizedBox(height: 102 * hr),
                    // Logo Card
                    Container(
                        width: 300 * wr,
                        height: 173 * hr,
                        decoration: BoxDecoration(
                            // Set the card to have rounded corners with a white background
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0 * sr)),
                            color: Colors.white),
                        child: Row(
                          // The card has a row of spacing, text, and the logo
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              'assets/logo.png',
                              width: 52 * wr,
                              height: 52 * hr,
                              fit: BoxFit.fill,
                            ),
                            SizedBox(width: 5 * wr),
                            Text(TITLE,
                                style: TextStyle(
                                  color: TEXT_APPNAME,
                                  fontFamily: DEFAULT_FONT_FAMILY,
                                  fontSize: 48 * sr,
                                )),
                            SizedBox(width: 5 * wr)
                          ],
                        )),
                    // Vertical Spacing
                    SizedBox(height: 58 * hr),
                    // Text Fields Card
                    Container(
                        height: 173 * hr,
                        width: 300 * wr,
                        decoration: BoxDecoration(
                            // Set the card to have rounded corners with a white background
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0 * sr)),
                            color: app_colors.SWATCH_B),
                        child: Padding(
                          // Padded container
                          padding: EdgeInsets.only(
                              top: 12 * hr, left: 15 * wr, right: 15 * wr),
                          child: Column(children: <Widget>[
                            // Column of text and typebars
                            Row(
                                // Sign In Text
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text('Sign In',
                                      style: TextStyle(
                                        fontFamily: DEFAULT_FONT_FAMILY,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18 * sr,
                                        color: TEXT_APPNAME
                                      ))
                                ]),
                            // Email typebar
                            typeBar(Icons.person_outline, 'Email', 6 * hr,
                                userController, false),
                            // Password typebar
                            typeBar(Icons.lock_outline, 'Password', 6 * hr,
                                passController, true),
                            // Vertical Spacing
                            SizedBox(height: 5 * hr),
                            Row(
                              // Forgot Password button
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                    height: 30 * hr,
                                    child: FlatButton(
                                        onPressed: () {
                                          Navigator.of(context)
                                              .pushNamed('/forgot');
                                        },
                                        padding: EdgeInsets.only(
                                            left: 10 * wr, right: 10 * wr),
                                        child: Text('Forgot Password',
                                            style: TextStyle(
                                              fontFamily: DEFAULT_FONT_FAMILY,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14,
                                              color: TEXT_APPNAME
                                            ))))
                              ],
                            )
                          ]),
                        )),
                    // Vertical Spacing
                    SizedBox(height: 19 * hr),
                    // SIGN IN Button
                    Container(
                        height: 51 * hr,
                        width: 300 * wr,
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15 * sr))),
                            color: app_colors.SWATCH_B,
                            onPressed: () =>
                                this.signIn(context), // Call the sign in method
                            child: Text('SIGN IN',
                                style: TextStyle(
                                    fontFamily: DEFAULT_FONT_FAMILY,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 28 * sr)))),
                    // "Don't have an account?" Button
                    Container(
                        width: 175 * wr,
                        child: FlatButton(
                            onPressed: () => Navigator.of(context).pushNamed(
                                '/signup'), // Navigate to the sign up page
                            child: Text(
                              "Don't have an account? Create one here",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: DEFAULT_FONT_FAMILY,
                                fontWeight: FontWeight.w500,
                                fontSize: 12 * sr,
                              ), // then the text isn't centered.
                            )))
                  ])));
        })));
  }

  // Method to display error message
  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text(
          'Invalid Email or Password',
          style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.bold,
              fontSize: 12 * sr),
        ),
        backgroundColor: app_colors.SWATCH_B,
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  // Sign in method
  void signIn(BuildContext context) async {
    if (await fbe.signIn(userController.text, passController.text) == 1) {
      // If the email and password is accepted
      storage.write(key: "username", value: userController.text);
      storage.write(key: "password", value: passController.text);
      p = await fbe.getProfile();
      StorageReference imageRef =
          FirebaseStorage.instance.ref().child('profiles/${fbu.uid}');
      String url = await imageRef.getDownloadURL();
      // Store user Information
      myUserImageURL = url;
      myUserImage = NetworkImage(url);
      myUserEmail = userController.text;
      Navigator.pushReplacementNamed(
          context, '/feedscreen'); // Navigate to Feed Screen
      myUsername = p.myName;
    } else {
      // If the email/password is incorrect call the error display
      _showToast(context);
    }
  }
}
