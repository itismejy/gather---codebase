import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';
import 'components.dart';
import 'colors.dart' as app_colors;
import 'classes.dart';
import 'text_styles.dart' as T;
import 'firebase_backend.dart' as fbe;

void main() => runApp(SettingsWidget());

class SettingsWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return SettingsPage();
  }
}

class SettingsPage extends StatefulWidget {
  final String username;
  final ImageProvider<dynamic> imageSource;
  final List<User> friends;
  bool darkMode = app_colors.nightMode;
  SettingsPage(
      {Key key, this.username, this.imageSource, this.friends, this.darkMode})
      : super(key: key);
  State<StatefulWidget> createState() {
    return Settings();
  }
}

class Settings extends State<SettingsPage> {
  void _showDialogBug(double width, double height) {
    TextEditingController bugMessage = new TextEditingController();

    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Send Feedback and Report Bugs",
              style: TextStyle(fontSize: 16 * sr)),
          content: Container(
              height: 80 * hr,
              child: Column(children: [
                TextField(
                  controller: bugMessage,
                  obscureText: false,
                  decoration:
                      InputDecoration(hintText: "Message to Developers"),
                ),
              ])),
          actions: <Widget>[
            new FlatButton(
                padding: EdgeInsets.only(right: 110 * wr),
                child: new Text("Cancel"),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
            SizedBox(width: 20 * wr),
            // usually buttons at the bottom of the dialog
            new FlatButton(
                child: new Text("Send"),
                onPressed: () async {
                  await fbe.createBug(bugMessage.text);
                  Navigator.of(context).pop();
                  _showToast(context, "Message successfully sent");
                }),
          ],
        );
      },
    );
  }

  bool darkMode = app_colors.nightMode;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  void _showToast(BuildContext context, String text) {
    final scaffold = _scaffoldKey.currentState;
    scaffold.showSnackBar(
      SnackBar(
        content: Text(
          text,
          style: TextStyle(
              color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12),
        ),
        backgroundColor: app_colors.SWATCH_B,
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: topBar(true, context,
            true), // Set the topBar to only display the back arrow
        backgroundColor: app_colors.SWATCH_B,
        body: Stack(children: <Widget>[
          Container(
              child: Column(children: [
            // Vertical Spacing
            SizedBox(height: 32 * hr),
            // Color Mode Button
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                height: 35 * hr,
                width: 320 * wr,
                child: FlatButton(
                  child: Container(
                      child: Text(
                          darkMode ? "Night Mode (Coming Soon!)" : "Day Mode",
                          style: T.small)),
                  onPressed: () {
                    // Change the Color Scheme
                    setState(() {
                      darkMode = !darkMode;
                    });
                    app_colors.nightMode = darkMode;
                  },
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(25.0 * sr)),
                    color: Colors.white),
              )
            ]),
            // Vertical Spacing
            SizedBox(height: 32 * hr),
            // Send feedback Button
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                height: 35 * hr,
                width: 320 * wr,
                child: FlatButton(
                  child: Container(
                      child: Text("What can we work on?", style: T.small)),
                  onPressed: () {
                    // Send User to Rate App
                    _showDialogBug(width, height);
                  },
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(25.0 * sr)),
                    color: Colors.white),
              )
            ]),
            // Vertical Spacing
            SizedBox(height: 8 * hr),
            // Rate this app Button
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                height: 35 * hr,
                width: 320 * wr,
                child: FlatButton(
                  child:
                      Container(child: Text("Rate This App", style: T.small)),
                  onPressed: () {
                    // Send User to Rate App
                    LaunchReview.launch();
                  },
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(25.0 * sr)),
                    color: Colors.white),
              )
            ]),
            // Vertical Spacing
            SizedBox(height: 32 * hr),
            // Sign Out Button
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                height: 35 * hr,
                width: 320 * wr,
                child: FlatButton(
                  child: Container(child: Text("Sign Out", style: T.smallRed)),
                  onPressed: () {
                    // Go back to the sign in page
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        '/signin', (Route<dynamic> route) => false);
                  },
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(25.0 * sr)),
                    color: Colors.white),
              )
            ])
          ]))
        ]));
  }
}
