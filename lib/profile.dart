import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'components.dart';
import 'colors.dart' as app_colors;
import 'classes.dart';
import 'firebase_backend.dart' as fbe;

void main() => runApp(ProfileWidget());

class ProfileWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return ProfilePage();
  }
}

class ProfilePage extends StatefulWidget {
  final String username;
  final ImageProvider<dynamic> imageSource;
  final List<User> friends;
  ProfilePage({Key key, this.username, this.imageSource, this.friends})
      : super(key: key);
  State<StatefulWidget> createState() {
    return YourProfile();
  }
}

class YourProfile extends State<ProfilePage> {
  String myName;
  ImageProvider<dynamic> imageSource;
  List<User> usernames;
  bool isReady = false;
  int incoming;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  initState() {
    super.initState();
  }

  Future<List<User>> setVariables() async {
    p = await fbe.getProfile();
    myName = p.myName;
    imageSource = myUserImage;
    usernames = p.friendnames;
    isReady = true;
    incoming = p.incoming;
    return usernames;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<User>>(
        future: setVariables(),
        builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text("Failed to retrieve feed");
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Scaffold(
                  backgroundColor: app_colors.SWATCH_B,
                  appBar: topBar(true, context, false),
                  body: SpinKitWanderingCubes(color: Colors.green));
            case ConnectionState.done:
              return build2(snapshot.data);
          }
          return null;
        });
  }

  Widget build2(List<User> usernames) {
    int listLength = 0;
    if (usernames != null) {
      listLength = usernames.length;
    }
    return Scaffold(
        backgroundColor: app_colors.SWATCH_B,
        appBar: topBar(true, context, false),
        resizeToAvoidBottomInset: true,
        key: _scaffoldKey,
        // body: SingleChildScrollView(child:Row(
        body: SingleChildScrollView(
            child: Container(
                width: width,
                height: height - 80 * hr,
                child: Stack(children: <Widget>[
                  Container(
                      child: Column(children: [
                    SizedBox(height: 25 * hr),
                    // Vertical Gap

                    // Vertical Gap

                    // Name + Icon
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      SizedBox(width: 90 * wr),
                      getProfileIcon(myName, imageSource),
                      Container(
                          height: 20 * hr,
                          width: 90 * wr,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                FlatButton(
                                    padding: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                        side: BorderSide.none),
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context, '/editprofile');
                                    },
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Icon(Icons.edit, size: 14),
                                          SizedBox(width: 2 * wr),
                                          Text(
                                            'Edit',
                                          )
                                        ])),
                              ])),
                    ]),
                    SizedBox(width: 10 * wr, height: 5 * hr),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(myName ?? '',
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                fontSize: 25.0 * sr,
                              ))
                        ]),
                    // Name + Icon
                    // Vertical Gap
                    Stack(alignment: Alignment.topRight, children: [
                      // Friends Header
                      Padding(
                          padding: EdgeInsets.only(
                              left: 20 * wr,
                              right: 20 * wr,
                              top: (1 / 100) * height * hr),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text("Friends",
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      fontSize: 20.0 * wr,
                                      color: Colors.black,
                                    )),
                                SizedBox(width: 8 * wr),
                                Text("(" + listLength.toString() + ")",
                                    textDirection: TextDirection.ltr,
                                    style: TextStyle(
                                      fontSize: 15.0 * wr,
                                      color: app_colors.HOVERING,
                                    )),
                                Spacer(),
                                Container(
                                  width: 70.0 * wr,
                                  height: 22.0 * hr,
                                  child: FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                      color: Color(0xff05D0FD),
                                      // color: Colors.red[100],
                                      padding: EdgeInsets.zero,
                                      child: Text("Manage",
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: app_colors.SWATCH_B)),
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context, '/add_friends');
                                      }),
                                )
                              ])),
                      (incoming > 0)
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                  Spacer(),
                                  Container(
                                      width: 14.0 * wr,
                                      height: 14.0 * hr,
                                      child: Text(incoming.toString(),
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: app_colors.SWATCH_B,
                                              fontSize: 10,
                                              fontWeight: FontWeight.bold)),
                                      // color: Color(0xff05D0FD),
                                      decoration: new BoxDecoration(
                                        color: Colors.green[700],
                                        shape: BoxShape.circle,
                                      )),
                                  SizedBox(
                                    width: 20 * wr,
                                  )
                                ])
                          : Container(),
                    ]),
                    // Friends Header

                    // Vertical Gap
                    SizedBox(height: 15 * hr),
                    // Vertical Gap

                    // Search
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: TextField(
                            onChanged: (newString) {
                              setState(() {
                                friendSearch = newString;
                              });
                            },
                            decoration: InputDecoration(
                              hintText: 'Search',
                              counterText: "",
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.fromLTRB(
                                  10.0 * wr, 10.0 * hr, 20.0 * wr, 10.0 * hr),
                              // border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                            ),
                          ))
                          //   decoration: InputDecoration(
                          //       hintText: 'Search', border: InputBorder.none),
                          // ))
                        ],
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(25.0)),
                        color: Colors.white,
                      ),
                      height: 30 * hr,
                      width: .9 * pwidth * wr,
                    ),
                    // Search

                    // Vertical Gap
                    SizedBox(height: 15),
                    // Vertical Gap

                    // Friend List
                    Expanded(child: renderFriendList(usernames))

                    // Friend List
                  ]))
                ]))));
  }

  String friendSearch = "";
  Widget getProfileIcon(String username, ImageProvider<dynamic> imageSource) =>
      Container(
          height: 150 * hr,
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
              width: 150 * wr,
              height: 150 * hr,
              child:
                  CircleAvatar(radius: 100 * wr, backgroundImage: imageSource),
            ),
          ]));

  List<User> queryName(List<User> friends) {
    if (friends == null) {
      return null;
    }
    return friends
        .where((name) => name.userName.startsWith(friendSearch))
        .toList();
  }

  void _showToast(BuildContext context, String text) {
    final scaffold = _scaffoldKey.currentState;
    scaffold.showSnackBar(
      SnackBar(
        content: Text(
          text,
          style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.bold,
              fontSize: 12 * wr),
        ),
        backgroundColor: app_colors.SWATCH_B,
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  Widget renderFriendList(List<User> friends) {
    if (friends == null) {
      return null;
    } else {
      return Container(
          width: (width - 35) * wr,
          padding: EdgeInsets.only(left: 0, right: 0),
          child: GridView.count(
              padding: EdgeInsets.zero,
              crossAxisCount: 3,
              children: queryName(friends)
                  .map((friendProf) => Center(
                          child: Column(children: [
                        Container(
                          width: 85 * wr,
                          height: 85 * wr,
                          child: CircleAvatar(
                              radius: 85 * wr,
                              backgroundImage: friendProf.userImage),
                        ),
                        SizedBox(height: 5 * hr),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(width: 12 * wr),
                              Container(
                                  width: 72.5 * wr,
                                  child: Text(friendProf.userName,
                                      overflow: TextOverflow.ellipsis,
                                      textAlign: TextAlign.center,
                                      textDirection: TextDirection.ltr,
                                      style: TextStyle(
                                        fontSize: 12.0 * wr,
                                      ))),
                              Container(
                                width: 12 * wr,
                                height: 12.0 * wr,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                ),
                                child: IconButton(
                                    padding: EdgeInsets.zero,
                                    icon: Icon(Icons.cancel,
                                        color: Colors.red, size: 12 * wr),
                                    onPressed: () {
                                      fbe.removeFriend(friendProf.userID);
                                      setState(() {
                                        friends.remove(friendProf);
                                      });
                                      _showToast(
                                          context,
                                          friendProf.userName +
                                              " was removed as a friend");
                                    }),
                              ),
                            ])
                      ])))
                  .toList()));
    }
  }
}
