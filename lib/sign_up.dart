import 'package:flutter/material.dart';
import 'package:grouper/text_styles.dart' as T;
import 'package:grouper/text_styles.dart';
import 'colors.dart' as app_colors;
import 'colors.dart';
import 'strings.dart';
import 'components.dart';
import 'firebase_backend.dart' as fbe;

class SignUp extends StatefulWidget {
  State<StatefulWidget> createState() {
    return SignUpPage();
  }
}

class SignUpPage extends State<SignUp> {
  // Vars for the typebars
  TextEditingController userController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  bool isInvisible = true;

  String user, password;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Method to change whether password is visible or not
  void _toggle() {
    setState(() {
      isInvisible = !isInvisible;
    });
  }

  // Widget for the bar to store user information
  Widget typeBar(IconData icon, String text, double pad,
      TextEditingController controller, bool isPassword) {
    return Padding(
        padding: EdgeInsets.only(top: pad), // Vertical Padding
        child: Container(
          // Container that holds the typebar
          height: 40 * hr,
          width: 270 * wr,
          child: Row(
              // Row of typebar elements
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 5 * wr), // Horizontal Spacing
                Icon(icon, color: app_colors.TEXT_APPNAME), // Specified Icon
                SizedBox(width: 5 * wr), // Horizontal Spacing
                Expanded(
                  // Container for text
                  child: TextField(
                      // Widget to enter text
                      controller: controller,
                      obscureText: (isPassword &&
                          isInvisible), // Check to see if the text should be hidden
                      decoration: InputDecoration.collapsed(
                          hintText: text) // Display the prompt
                      ),
                ),
                (isPassword)
                    ? SizedBox(
                        // If it is a password typebar display the red eye icon
                        width: 40 * wr,
                        child: IconButton(
                            padding: EdgeInsets.zero,
                            icon: Icon(Icons.remove_red_eye,
                                color: app_colors.TEXT_APPNAME),
                            onPressed: () {
                              _toggle();
                            } // Call the toggle to change the password display
                            ))
                    : SizedBox(
                        width: 10 *
                            wr) // If it is not a password typebar just add spacing
              ]),
          decoration: BoxDecoration(
              // Make the typebar be white with rounded corners
              borderRadius: BorderRadius.all(Radius.circular(10.0 * sr)),
              color: Colors.white),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Builder(builder: (context) {
          return SingleChildScrollView(
              // Page is scrollable - for when the keyboard pops up
              child: Container(
                  width: width,
                  height: height,
                  // Background Gradient
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            app_colors.GRADIENT_A,
                            app_colors.GRADIENT_B
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [0.0, 1.0],
                          tileMode: TileMode.clamp)),
                  // Main column for layout
                  child: Column(children: <Widget>[
                    // Vertical Spacing
                    SizedBox(height: 102 * hr),
                    // Logo Card
                    Container(
                        width: 300 * wr,
                        height: 173 * hr,
                        decoration: BoxDecoration(
                            // Set the card to have rounded corners with a white background
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0 * sr)),
                            color: Colors.white),
                        child: Row(
                          // The card has a row of spacing, text, and the logo
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              'assets/logo.png',
                              width: 52 * wr,
                              height: 52 * hr,
                              fit: BoxFit.fill,
                            ),
                            SizedBox(width: 5 * wr),
                            Text(TITLE,
                                style: TextStyle(
                                  color: TEXT_APPNAME,
                                  fontFamily: DEFAULT_FONT_FAMILY,
                                  fontSize: 48 * sr,
                                )),
                            SizedBox(width: 5 * wr)
                          ],
                        )),
                    // Vertical Spacing
                    SizedBox(height: 38 * hr),
                    // Text Fields Card
                    Container(
                        height: 192 * hr,
                        width: 300 * wr,
                        decoration: BoxDecoration(
                            // Set the card to have rounded corners with a white background
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0 * sr)),
                            color: app_colors.SWATCH_B),
                        child: Padding(
                          // Padded Container
                          padding: EdgeInsets.only(
                              top: 12 * hr, left: 15 * wr, right: 15 * wr),
                          child: Column(children: <Widget>[
                            Row(
                                // Sign Up Text
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text('Sign Up',
                                      style: TextStyle(
                                        fontFamily: DEFAULT_FONT_FAMILY,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18 * sr,
                                        color: TEXT_APPNAME
                                      ))
                                ]),
                            // Name typebar
                            typeBar(Icons.person_outline, 'Name', 6 * hr,
                                userController, false),
                            // Email typebar
                            typeBar(Icons.mail_outline, 'Email', 6 * hr,
                                emailController, false),
                            // Password typebar
                            typeBar(Icons.lock_outline, 'Password', 6 * hr,
                                passController, true),
                          ]),
                        )),
                    // Vertical Spacing
                    SizedBox(height: 19 * hr),
                    // SIGN UP Button
                    Container(
                        height: 51 * hr,
                        width: 300 * wr,
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15 * sr))),
                            color: app_colors.SWATCH_B,
                            onPressed: () => this.signUp(context),
                            child: Text('SIGN UP',
                                style: TextStyle(
                                  fontFamily: DEFAULT_FONT_FAMILY,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 28 * sr,
                                  color: TEXT_APPNAME
                                )))),
                    // "Already have account?" Button
                    Container(
                        width: 180 * wr,
                        child: FlatButton(
                            onPressed: () => Navigator.of(context).pop(),
                            child: Text(
                              "Already have an account? Log in",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 12.0 * sr),
                            )))
                  ])));
        }));
  }

  // Method to display error message
  void _showToast(BuildContext context, String text) {
    final scaffold = _scaffoldKey.currentState;
    scaffold.showSnackBar(
      SnackBar(
        content: Text(
          text,
          style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.bold,
              fontSize: 12 * sr),
        ),
        backgroundColor: app_colors.SWATCH_B,
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  // Sign Up Method
  void signUp(BuildContext context) async {
    // Cacth user entry errors and call the error display
    if (userController.text.length == 0) {
      _showToast(context, '"Name" cannot be empty');
    } else if (emailController.text.length == 0) {
      _showToast(context, '"Email" cannot be empty');
    } else if (passController.text.length < 6) {
      _showToast(context, '"Password" should be at least 6 characters');
    }
    // Create the account
    else {
      int result = await fbe.createAccount(
          userController.text, passController.text, emailController.text);
      if (result == 0) {
        // If the account creation if successfull, go to the sign in screen
        Navigator.of(context).pop();
      }
      // If there is an error in creation
      else if (result == 1) {
        _showToast(context, 'The email address is formatted improperly');
      } else {
        _showToast(context, 'An account with this email already exists');
      }
    }
  }
}
