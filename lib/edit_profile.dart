import 'package:flutter/material.dart';
import 'colors.dart' as app_colors;
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'classes.dart';
import 'components.dart';
import 'firebase_backend.dart' as fbe;

void main() => runApp(EditWidget());

class EditWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return Edit();
  }
}

class Edit extends StatefulWidget {
  State<StatefulWidget> createState() {
    return EditPage();
  }
}

class EditPage extends State<Edit> {
  TextEditingController oldPassController = new TextEditingController();
  TextEditingController newPassController = new TextEditingController();
  TextEditingController newPass2Controller = new TextEditingController();

  final oldPassKey = GlobalKey<FormState>();
  final newPassKey = GlobalKey<FormState>();
  final newPass2Key = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  File file;
  bool isWrong = false;
  bool isInvisible1 = true;
  bool isInvisible2 = true;
  bool isInvisible3 = true;

  Future<int> _choose() async {
    file = await ImagePicker.pickImage(source: ImageSource.gallery);
    await fbe.editImage(file);
    // setState(() {});
    // Navigator.of(context).pushNamedAndRemoveUntil(
    //     '/feedscreen', (Route<dynamic> route) => false);
    if (file != null) {
      return 0;
    }
    return 1;
  }

  void _toggle(String whatP) {
    setState(() {
      if (whatP == "is1") {
        isInvisible1 = !isInvisible1;
      }
      if (whatP == "is2") {
        isInvisible2 = !isInvisible2;
      }
      if (whatP == "is3") {
        isInvisible3 = !isInvisible3;
      }
    });
  }

  Widget getProfileIcon(String username, ImageProvider<dynamic> imageSource) {
    // if (imageSource == null) {
    //   imageSource = new Image(image: AssetImage("assets/background_image.jpg"));
    //   // imageSource=new Image(image: myUserImage);
    // }p
    return Container(
        height: 150 * hr,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            SizedBox(width: 40 * wr),
            Container(
              width: 150 * wr,
              height: 150 * hr,
              child:
                  CircleAvatar(radius: 100 * sr, backgroundImage: imageSource),
            ),
            SizedBox(width: 10 * wr),
            Column(children: <Widget>[
              Container(
                  width: 30 * wr,
                  height: 30 * hr,
                  child: IconButton(
                      padding: EdgeInsets.zero,
                      icon:
                          Icon(Icons.edit, color: Colors.black, size: 20 * sr),
                      onPressed: () async {
                        await _choose();
                        Navigator.of(context).pop();
                        Navigator.popAndPushNamed(context, '/profile');
                        Navigator.pushNamed(context, '/editprofile');
                      }))
            ])
          ]),
        ]));
  }

  Widget typeBar(
      IconData icon,
      String text,
      final key,
      double pad,
      double width,
      double height,
      TextEditingController controller,
      bool isPassword,
      bool isV,
      String whatP) {
    return (Padding(
        padding: EdgeInsets.only(top: pad),
        child: Container(
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 5 * wr),
                Icon(icon, color: app_colors.TEXT_APPNAME),
                SizedBox(width: 5 * wr),
                Expanded(
                  child: TextFormField(
                      controller: controller,
                      obscureText: isPassword && isV,
                      decoration: InputDecoration.collapsed(hintText: text)),
                ),
                (isPassword)
                    ? SizedBox(
                        width: 40 * wr,
                        child: IconButton(
                            padding: EdgeInsets.zero,
                            icon: Icon(Icons.remove_red_eye,
                                color: app_colors.TEXT_APPNAME),
                            onPressed: () {
                              _toggle(whatP);
                              setState(() {});
                            }))
                    : SizedBox(width: 10 * wr),
              ]),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(25.0 * sr)),
              color: Colors.white),
          height: 40 * hr,
          width: 270 * wr,
        )));
  }

  void _showToast(BuildContext context, String text) {
    final scaffold = _scaffoldKey.currentState;
    scaffold.showSnackBar(
      SnackBar(
        content: Text(
          text,
          style: TextStyle(
              color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12),
        ),
        backgroundColor: app_colors.SWATCH_B,
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  void _showDialogUser(double width, double height) {
    TextEditingController userC = new TextEditingController();

    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Change Name", style: TextStyle(fontSize: 16 * sr)),
          content: Container(
              height: 80 * hr,
              child: Column(children: [
                TextField(
                  controller: userC,
                  obscureText: false,
                  decoration: InputDecoration(hintText: "New Name"),
                ),
              ])),
          actions: <Widget>[
            new FlatButton(
                padding: EdgeInsets.only(right: 110 * wr),
                child: new Text("Cancel"),
                onPressed: () {
                  isWrong = false;
                  Navigator.of(context).pop();
                }),
            SizedBox(width: 20 * wr),
            // usually buttons at the bottom of the dialog
            new FlatButton(
                child: new Text("Confirm"),
                onPressed: () async {
                  await fbe.editName(userC.text);
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/feedscreen', (Route<dynamic> route) => false);
                  Navigator.pushNamed(context, '/profile');
                  Navigator.pushNamed(context, '/editprofile');
                }),
          ],
        );
      },
    );
  }

  void _showDialogEmail(double width, double height) {
    TextEditingController emailC = new TextEditingController();

    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Send Reset Password Email",
              style: TextStyle(fontSize: 16 * sr)),
          content: Container(
              height: 80 * hr,
              child: Column(children: [
                TextField(
                  controller: emailC,
                  obscureText: false,
                  decoration: InputDecoration(hintText: "Email Address"),
                ),
              ])),
          actions: <Widget>[
            new FlatButton(
                padding: EdgeInsets.only(right: 110 * wr),
                child: new Text("Cancel"),
                onPressed: () {
                  isWrong = false;
                  Navigator.of(context).pop();
                }),
            SizedBox(width: 20 * wr),
            // usually buttons at the bottom of the dialog
            new FlatButton(
                child: new Text("Send"),
                onPressed: () async {
                  if (myUserEmail == emailC.text) {
                    bool isSuccessful = await fbe.forgotPassword(emailC.text);
                  } else {
                    _showToast(context, "Incorrect Email Address");
                  }
                }),
          ],
        );
      },
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: topBar(true, context, false),
        backgroundColor: app_colors.SWATCH_B,
        key: _scaffoldKey,
        body: Builder(builder: (context) {
          return SingleChildScrollView(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(height: 25 * hr),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      file == null
                          ? getProfileIcon(myUsername, myUserImage)
                          : getProfileIcon(myUsername, FileImage(file))
                    ]),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            SizedBox(width: 40 * wr),
            Container(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(myUsername ?? '',
                      overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                            fontSize: 25.0 * sr,
                          ))
                    ])),
            SizedBox(width: 10 * wr),
            Column(children: <Widget>[
              Container(
                  width: 30 * wr,
                  height: 30 * hr,
                  child: IconButton(
                      padding: EdgeInsets.zero,
                      icon:
                          Icon(Icons.edit, color: Colors.black, size: 20 * sr),
                      onPressed: () async {
                        _showDialogUser(
                          width,
                          height,
                        );
                      }))
            ])
          ]),
                    SizedBox(height: 10.0 * hr),
                    Container(
                      child: FlatButton(
                        child: Container(child: Text("Change Password")),
                        onPressed: () {
                          _showDialogEmail(
                            width,
                            height,
                          );
                        },
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(25.0)),
                          color: Colors.white),
                      height: 35 * hr,
                      width: .9 * getWidth(),
                    )
                  ],
                ),
              ]));
        }));
  }
}
