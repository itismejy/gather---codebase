import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'colors.dart' as app_colors;
import 'components.dart';
import 'package:url_launcher/url_launcher.dart';
import 'chatroom2.dart';
import 'edit_info.dart';
import 'classes.dart';
import 'text_styles.dart' as T;
import 'firebase_backend.dart' as fbe;

// https://stackoverflow.com/questions/49345605/how-to-open-an-application-from-a-flutter-app

void main() => runApp(FurtherInfo());

AttendanceState thisStatus;

class FurtherInfo extends StatelessWidget {
  final String thisID;
  final bool isPast;
  FurtherInfo({Key key, this.thisID, this.isPast}) : super(key: key);
  Widget build(BuildContext context) {
    return FutureBuilder<Event>(
        future: fbe.getEventDetails(thisID, isPast),
        builder: (BuildContext context, AsyncSnapshot<Event> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text("Failed to retrieve feed");
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Scaffold(
                  backgroundColor: app_colors.SWATCH_B,
                  appBar: topBar(true, context, false),
                  body: SpinKitWanderingCubes(color: Colors.green));
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text("Error: ${snapshot.error}");
              }
              return FurtherState(event: snapshot.data, isPast: isPast);
          }
          return null;
        });
  }
}

//Map<User, AttendanceState> map = new Map<User, AttendanceState>();
//User f = User("jason", AssetImage("assets/background_image.jpg"));

class FurtherState extends StatefulWidget {
  final Event event;
  final bool isPast;
  FurtherState({Key key, this.event, this.isPast}) : super(key: key);
  State<StatefulWidget> createState() {
    return FurtherInfoPage(event: event, isPast: isPast);
  }
}

class GenerateProfiles extends StatelessWidget {
  final User user;
  GenerateProfiles({Key key, this.user}) : super(key: key);
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(right: 20 * wr),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(user.userName),
          Container(
              padding: EdgeInsets.only(left: 10 * wr),
              width: 30.0 * wr,
              height: 30.0 * wr,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image:
                      DecorationImage(fit: BoxFit.fill, image: user.userImage)))
        ]));
  }
}

class FurtherInfoPage extends State<FurtherState> {
  final List<String> _items = ['Going', 'Busy', 'Undecided'].toList();
  Event event;
  bool isPast;

  FurtherInfoPage({this.event, this.isPast});

  String _selection = 'Undecided';

  @override
  void initState() {
    //this only works when feed_screen.dart is rebuilt, needs to be fixed
    super.initState();
    if (event.getAttendanceState() == AttendanceState.UNDECIDED) {
      _selection = _items[2];
    } else if (event.getAttendanceState() == AttendanceState.GOING) {
      _selection = _items[0];
    } else {
      _selection = _items[1];
    }
  }

  _openMap(String address) async {
    address.replaceAll(new RegExp(r' '), '+');
    String url = "https://www.google.com/maps/place/" + address;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget getProfileIcon(String username, ImageProvider<dynamic> imageSource) =>
      // Host Profile Image
      Padding(
          padding: EdgeInsets.only(top: 10 * hr, bottom: 0),
          child: Row(children: [
            Container(
                width: 30.0 * wr,
                height: 30.0 * wr,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image:
                        DecorationImage(fit: BoxFit.fill, image: imageSource))),
            (!isPast)
                ? Text("  " + username + " is hosting!  ",
                    style: TextStyle(
                      fontSize: 20.0 * wr,
                    ))
                : Text("  " + username + " hosted this!  ",
                    style: TextStyle(
                      fontSize: 20.0 * wr,
                    ))
          ]));
  String dropdownValue = 'One';

  bool isUserEvent(Event event) {
    if (event.host.userName == myUsername) {
      return true;
    } else {
      return false;
    }
  }

  Widget bottomButton(Event event, bool isNew, Size size) {
    return (!isPast)
        ? Container(
            height: 45 * hr,
            width: 120 * wr,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(25 * wr))),
              color: Colors.white,
              child: Text('Edit Event', style: T.medium),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            EditInfo(newEvent: event, isNew: isNew)));
              },
            ))
        : Container(
            height: 45 * hr,
            width: 120 * wr,
            padding: EdgeInsets.zero,
            child: RaisedButton(
                elevation: 4,
                color: app_colors.HEADER,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25 * wr))),
                onPressed: () async {
                  await fbe.deleteEvent(event.id, true);
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/feedscreen', (Route<dynamic> route) => false);
                },
                child: Row(
                  children: <Widget>[Text("Delete")],
                  mainAxisSize: MainAxisSize.min,
                )));
  }

  Widget dropDown(List<DropdownMenuItem<String>> dropdownMenuOptions) {
    return (isPast)
        ? Container(
            height: 45 * hr,
            width: 120 * wr,
            padding: EdgeInsets.zero,
            child: RaisedButton(
                elevation: 4,
                color: app_colors.HEADER,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25 * wr))),
                onPressed: () async {
                  await fbe.leaveEvent(event.id);
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/feedscreen', (Route<dynamic> route) => false);
                },
                child: Row(
                  children: <Widget>[Text("Leave")],
                  mainAxisSize: MainAxisSize.min,
                )))
        : Container(
            height: 40 * hr,
            width: 120 * wr,
            decoration: ShapeDecoration(
              shadows: [
                BoxShadow(
                  color: Colors.grey[350],
                  blurRadius: 1.0, // has the effect of softening the shadow
                  spreadRadius: 0.001, // has the effect of extending the shadow
                  offset: Offset(
                    0.0, // horizontal, move right 10
                    03.0 * hr, // vertical, move down 10
                  ),
                ),
              ],
              // shadows: [BoxShadow(color:Colors.black)],
              color: app_colors.HEADER,
              shape: RoundedRectangleBorder(
                // decoration: color: app_colors.HEADER,
                side: BorderSide(
                    width: 1.0 * wr,
                    color: app_colors.HEADER,
                    style: BorderStyle.solid),
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
              ),
            ),
            padding: EdgeInsets.only(left: 10 * wr),
            child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                    elevation: 4,
                    value: _selection,
                    items: dropdownMenuOptions,
                    onChanged: (s) async {
                      setState(() {
                        _selection = s;
                      });
                      await fbe.changeStatus(
                          event.id, _selection.toLowerCase());
                      setState(() {
                        event.attendees.remove(current);
                        event.attendees[current] =
                            toAttendanceState(_selection.toLowerCase());
                      });
                    })));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final dropdownMenuOptions = _items
        .map((String item) =>
            new DropdownMenuItem<String>(value: item, child: new Text(item)))
        .toList();
    List<String> list = getDayAndTime(event.start, event.end);

    List<User> goingUsers = [];
    List<User> undecidedUsers = [];
    List<User> busyUsers = [];
    event.attendees.forEach((user, attendanceState) {
      switch (attendanceState) {
        case AttendanceState.UNDECIDED:
          undecidedUsers.add(user);
          break;
        case AttendanceState.BUSY:
          busyUsers.add(user);
          break;
        case AttendanceState.GOING:
          goingUsers.add(user);
          break;
      }
    });
    var goingList = goingUsers
        .map((user) => GenerateProfiles(user: user))
        .cast<Widget>()
        .toList();
    var busyList = busyUsers
        .map((user) => GenerateProfiles(user: user))
        .cast<Widget>()
        .toList();
    var undecidedList = undecidedUsers
        .map((user) => GenerateProfiles(user: user))
        .cast<Widget>()
        .toList();
    return Scaffold(
        appBar: topBar(true, context, false),
        backgroundColor: app_colors.BACKGROUND,
        body: Stack(children: <Widget>[
          Card(
              color: app_colors.SWATCH_B,
              clipBehavior: Clip.hardEdge,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: Padding(
                  padding: EdgeInsets.only(
                      bottom: 10 * hr, left: 10 * wr, right: 10 * wr),
                  child: Column(
                    children: [
                      SizedBox(height: 5 * hr),
                      // Spacing
                      Container(
                          height: 40 * hr,
                          child: Text("" + this.event.title,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 26.0 * wr,
                              ))),
                      // Meeting Details
                      Card(
                          color: app_colors.SWATCH_B,
                          elevation: 4,
                          clipBehavior: Clip.hardEdge,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          child: Container(
                            width: size.width,
                            padding: EdgeInsets.symmetric(horizontal: 10 * wr),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                              color: Colors.white.withAlpha(150),
                            ),
                            child: Column(children: [
                              SizedBox(height: 10 * hr),
                              Row(children: [
                                Icon(Icons.calendar_today),
                                SizedBox(width: 15 * wr),
                                Text(list[0]) //should be datetime object
                              ]),
                              SizedBox(height: 15 * hr),
                              Row(children: [
                                Icon(Icons.access_time),
                                SizedBox(width: 15 * wr),
                                Text(list[1]) //should be endtime object
                              ]),
                              SizedBox(height: 3 * hr),
                              Row(children: [
                                Icon(Icons.place),
                                SizedBox(width: 15 * wr),
                                Container(
                                    width: 210 * wr,
                                    child: Text(event.location)),
                                IconButton(
                                    padding: EdgeInsets.zero,
                                    icon: Icon(Icons.map,
                                        color: Color(0xff797979),
                                        size: 30 * wr),
                                    onPressed: () => _openMap(event.location))
                              ]),
                            ]),
                          )),
                      // Spacing
                      SizedBox(height: 20 * hr),
                      // People
                      Card(
                        elevation: 4,
                        color: app_colors.SWATCH_B,
                        clipBehavior: Clip.hardEdge,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Container(
                            height: 250 * hr,
                            width: size.width,
                            // margin: EdgeInsets.only(bottom: 10), //used to have top: 180
                            padding: EdgeInsets.zero,
                            child: Padding(
                                padding: EdgeInsets.only(left: 10 * wr),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(children: [
                                        getProfileIcon(event.host.userName,
                                            event.host.userImage),
                                      ]),
                                      SizedBox(height: 10 * hr),
                                      // Text("Going:",
                                      //     style: TextStyle(
                                      //       fontSize: 15.0,
                                      //     )),
                                      SizedBox(height: 10 * hr),
                                      Row(children: [
                                        (!isPast)
                                            ? Text("Going:  ",
                                                style: TextStyle(
                                                  fontSize: 15.0 * wr,
                                                ))
                                            : Text("Went:  ",
                                                style: TextStyle(
                                                  fontSize: 15.0 * wr,
                                                )),
                                        SizedBox(width: 10 * wr),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                height: 50 * hr,
                                                width: 233 * wr,
                                                child: ListView(
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    shrinkWrap: true,
                                                    children: goingList),
                                              )
                                            ]),
                                      ]),
                                      SizedBox(height: 10 * hr),
                                      Row(children: [
                                        (!isPast)
                                            ? Text("Busy:  ",
                                                style: TextStyle(
                                                  fontSize: 15.0 * wr,
                                                ))
                                            : Text("Was Busy:  ",
                                                style: TextStyle(
                                                  fontSize: 15.0 * wr,
                                                )),
                                        SizedBox(height: 10 * hr),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                height: 50 * hr,
                                                width: 230 * wr,
                                                child: ListView(
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    shrinkWrap: true,
                                                    children: busyList),
                                              )
                                            ]),
                                      ]),
                                      SizedBox(height: 10 * hr),
                                      Row(children: [
                                        (!isPast)
                                            ? Text("Undecided:  ",
                                                style: TextStyle(
                                                  fontSize: 15.0 * wr,
                                                ))
                                            : Text("Was Undecided:  ",
                                                style: TextStyle(
                                                  fontSize: 15.0 * wr,
                                                )),
                                        Row(children: [
                                          Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                SizedBox(
                                                  height: 50 * hr,
                                                  width: 190 * wr,
                                                  child: ListView(
                                                      scrollDirection:
                                                          Axis.horizontal,
                                                      shrinkWrap: true,
                                                      children: undecidedList),
                                                )
                                              ]),
                                        ])
                                      ]),
                                    ])),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                              color: Colors.white.withAlpha(150),
                            )),
                      )
                    ],
                  ))),
          Align(
              alignment: Alignment.bottomLeft,
              child: Row(children: [
                Container(
                    height: 45 * wr,
                    width: 45 * wr,
                    margin: EdgeInsets.all(20 * wr),
                    padding: EdgeInsets.zero,
                    child: RaisedButton(
                        padding: EdgeInsets.symmetric(
                            vertical: 8 * hr, horizontal: 8 * wr),
                        color: app_colors.HEADER,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(30))),
                        onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  Chat(messagesID: event.messagesID),
                            )),
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.chat_bubble_outline, size: 20 * hr),
                          ],
                          mainAxisSize: MainAxisSize.min,
                        ))),
                SizedBox(width: 140 * wr),
                (isUserEvent(event)
                    ? bottomButton(event, false, size)
                    : dropDown(dropdownMenuOptions))
              ]))
        ]));
  }
}
