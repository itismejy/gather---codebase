import 'package:flutter/material.dart';
import 'package:grouper/components.dart';
import 'package:grouper/text_styles.dart';
import 'colors.dart' as app_colors;
import 'colors.dart';
import 'strings.dart';
import 'text_styles.dart' as T;
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'firebase_backend.dart' as fbe;

void main() => runApp(ForgotWidget());

class ForgotWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return Forgot();
  }
}

class Forgot extends StatefulWidget {
  State<StatefulWidget> createState() {
    return ForgotPage();
  }
}

Widget typeBar(IconData icon, String text, final key, double pad, double width,
    double height, TextEditingController controller) {
  print(wr);
  print(hr);
  return (Padding(
      padding: EdgeInsets.only(top: pad),
      child: Container(
        child:
            Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          SizedBox(width: 5 * wr),
          Icon(icon, color: app_colors.TEXT_APPNAME),
          SizedBox(width: 5 * wr),
          Form(
              key: key,
              child: Expanded(
                child: TextFormField(
                    controller: controller,
                    decoration: InputDecoration.collapsed(hintText: text)),
              )),
        ]),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            color: Colors.white),
        height: 40 * hr,
        width: 270 * wr,
      )));
}

class ForgotPage extends State<Forgot> {
  TextEditingController emailController = new TextEditingController();

  final emailKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    //Size SIZE = MediaQuery.of(context).size;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        key: _scaffoldKey,
        body: SingleChildScrollView(
            child: Container(
                width: width,
                height: height,
                // Background Gradient
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [app_colors.GRADIENT_A, app_colors.GRADIENT_B],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp)),
                // Main column for layout
                child: Column(children: <Widget>[
                  // Padding
                  SizedBox(height: (16 / 100) * height),
                  // Logo Card
                  Container(
                      width: (5 / 6) * width,
                      height: (27 / 100) * height,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          color: Colors.white),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            'assets/logo.png',
                            width: 52, // These should adjust by screen size
                            height: 52,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(
                              width: 5), // width should adjust to screen width
                          Text(TITLE, style: TextStyle(
  color: TEXT_APPNAME,
  fontFamily: DEFAULT_FONT_FAMILY,
  fontSize: 48 * sr,
)),
                          SizedBox(width: 5) // width should adjus
                        ],
                      )),
                  // Padding
                  SizedBox(height: (15 / 100) * height),
                  // Text Fields Card
                  Container(
                      height: (21 / 100) * height,
                      width: (5 / 6) * width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          color: app_colors.SWATCH_B),
                      child: Padding(
                          padding:
                              EdgeInsets.only(top: 12, left: 15, right: 15),
                          child: Column(children: <Widget>[
                            // Header Text
                            Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text('Forgot Password', style: TextStyle(
  fontFamily: DEFAULT_FONT_FAMILY,
  fontWeight: FontWeight.w500,
  fontSize: 18 * sr,
  color: TEXT_APPNAME
)),
                                ]),
                            // Email
                            typeBar(
                                Icons.mail_outline,
                                'Email',
                                emailKey,
                                (1 / 100) * height,
                                width,
                                height,
                                emailController),
                            SizedBox(height: 5),
                            Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                      width: 200 * wr,
                                      child: Text(
                                          'We\'ll send you an email to reset your password',
                                          textAlign: TextAlign.end,
                                          style: 
TextStyle(
  fontFamily: DEFAULT_FONT_FAMILY,
  fontWeight: FontWeight.w500,
  fontSize: 14,
  color: TEXT_APPNAME
)
))
                                ])
                          ]))),
                  // Padding
                  SizedBox(height: (3 / 100) * height),
                  // SEND EMAIL Button
                  Container(
                      height: (8 / 100) * height,
                      width: (5 / 6) * width,
                      child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          color: app_colors.SWATCH_B,
                          onPressed: () async {
                            bool isSuccessful = await fbe.forgotPassword(
                                emailController.text.toLowerCase().trim());
                            if (isSuccessful) {
                              _showToast(context,
                                  "Password reset email successfully sent");
                            } else {
                              _showToast(
                                  context, "No account exists with this email");
                            }
                          },
                          child: Text('SEND EMAIL', style: TextStyle(
  fontFamily: DEFAULT_FONT_FAMILY,
  fontWeight: FontWeight.w500,
  fontSize: 28 * sr,
  color: TEXT_APPNAME
                  )))),
                  // 'Remembered your login?'
                  Container(
                      width: (50 / 100) * width,
                      child: FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text('Remembered your login? Log In',
                              textAlign: TextAlign.center,
                              style: TextStyle(
  fontFamily: DEFAULT_FONT_FAMILY,
  fontWeight: FontWeight.w500,
  fontSize: 12 * sr,
)))),
                ]))));
  }

  void _showToast(BuildContext context, String text) {
    final scaffold = _scaffoldKey.currentState;
    scaffold.showSnackBar(
      SnackBar(
        content: Text(
          text,
          style: TextStyle(
              color: Colors.red, fontWeight: FontWeight.bold, fontSize: 12),
        ),
        backgroundColor: app_colors.SWATCH_B,
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  void sendEmail(String email, String user, String pass) async {
    String username = 'gather.helpme@gmail.com';
    String password = 'rehtag1234';
    final smtpServer = gmail(username, password);
    final message = Message()
      ..from = Address(username, 'Gather Support')
      ..recipients.add(email)
      ..subject = 'Your Gather Username and Password'
      ..text
      ..html = '<h1>Your username is ' +
          user +
          '.\nYour new auto-generated password is ' +
          pass +
          '.</h1>\n<p>\n';

    try {
      await send(message, smtpServer);
    } catch (e) {
      print(e);
    }
  }
}
