import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:grouper/classes.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'FullPhoto.dart';
import 'components.dart';
import 'colors.dart' as app_colors;

class Chat extends StatelessWidget {
  final String messagesID;

  Chat({Key key, @required this.messagesID}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: topBar(true, context, false),
      backgroundColor: app_colors.BACKGROUND,
      body: new ChatScreen(messagesID: messagesID),
    );
  }
}

class ChatScreen extends StatefulWidget {
  String messagesID;

  ChatScreen({Key key, @required this.messagesID}) : super(key: key);

  @override
  State createState() => new ChatScreenState(messagesID: messagesID);
}

class ChatScreenState extends State<ChatScreen> {
  ChatScreenState({Key key, @required this.messagesID});

  String messagesID;

  var listMessage;

  File imageFile;
  bool isLoading;
  bool isShowSticker;
  String imageUrl;

  final TextEditingController textEditingController =
      new TextEditingController();
  final ScrollController listScrollController = new ScrollController();
  final FocusNode focusNode = new FocusNode();

  @override
  void initState() {
    super.initState();
    focusNode.addListener(onFocusChange);
    //messagesID = "";
    isLoading = false;
    isShowSticker = false;
    imageUrl = '';
  }

  void onFocusChange() {
    if (focusNode.hasFocus) {
      // Hide sticker when keyboard appear
      setState(() {
        isShowSticker = false;
      });
    }
  }

  Future getImage() async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (imageFile != null) {
      setState(() {
        isLoading = true;
      });
      uploadFile();
    }
  }

  void getSticker() {
    // Hide keyboard when sticker appear
    focusNode.unfocus();
    setState(() {
      isShowSticker = !isShowSticker;
    });
  }

  Future uploadFile() async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference reference = FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = reference.putFile(imageFile);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
    storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
      imageUrl = downloadUrl;
      setState(() {
        isLoading = false;
        onSendMessage(imageUrl, 1);
      });
    }, onError: (err) {
      setState(() {
        isLoading = false;
      });
    });
  }

  void onSendMessage(String content, int type) {
    // type: 0 = text, 1 = image, 2 = sticker
    if (content.trim() != '') {
      textEditingController.clear();

      var documentReference = Firestore.instance
          .collection('messages')
          .document(messagesID)
          .collection(messagesID)
          .document(DateTime.now().millisecondsSinceEpoch.toString());

      Firestore.instance.runTransaction((transaction) async {
        await transaction.set(
          documentReference,
          {
            'idFrom': fbu.uid,
            'idTo': messagesID,
            'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
            'content': content,
            'type': type
          },
        );
      });
      listScrollController.animateTo(0.0,
          duration: Duration(milliseconds: 300), curve: Curves.easeOut);
    } else {
      // Fluttertoast.showToast(msg: 'Nothing to send');
    }
  }

  Widget buildItem(int index, DocumentSnapshot document) {
    return FutureBuilder<String>(
        future: getUserImage(document),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text("Failed to retrieve feed");
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Container();
            // return SpinKitCircle(color: app_colors.SWATCH_A);
            case ConnectionState.done:
              if (snapshot.hasError) {
                print("was an error with image!");
                return Text("Error: ${snapshot.error}");
              }
              return buildItem2(index, document, snapshot.data);
          }
          return null;
        });
  }

  Widget buildItem2(int index, DocumentSnapshot document, String url) {
    return FutureBuilder<String>(
        future: getUsername(document),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Text("Failed to retrieve feed");
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Container();
            // return SpinKitCircle(color: app_colors.SWATCH_A);
            case ConnectionState.done:
              if (snapshot.hasError) {
                return Text("Error: ${snapshot.error}");
              }
              return buildSingleItem(index, document, url, snapshot.data);
          }
          return null;
        });
  }

  Future<String> getUserImage(DocumentSnapshot document) async {
    String userID = await document['idFrom'];
    StorageReference imageRef =
        FirebaseStorage.instance.ref().child('profiles/$userID');
    String url = await imageRef.getDownloadURL();
    return url;
  }

  Future<String> getUsername(DocumentSnapshot document) async {
    var firestore = Firestore.instance;
    DocumentReference docRef =
        firestore.collection('users').document(document['idFrom']);
    String dd = "";
    await docRef.get().then((datasnapshot) async {
      dd = datasnapshot.data['username'];
    });
    return dd;
  }

  Widget buildSingleItem(
      int index, DocumentSnapshot document, String url, String userID) {
    if (document['idFrom'] == fbu.uid) {
      // Right (my message)
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          document['type'] == 0
              // Text
              ? Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
                  Container(
                    child: Text(
                      document['content'],
                    ),
                    padding: EdgeInsets.fromLTRB(
                        15.0 * wr, 10.0 * hr, 15.0 * wr, 10.0 * hr),
                    width: 200.0 * wr,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8.0)),
                    margin: EdgeInsets.only(
                        bottom: isLastMessageRight(index) ? 0.0 : 10.0 * hr,
                        right: 10.0 * wr),
                  ),
                  isLastMessageRight(index)
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                              Container(
                                child: Text(
                                  DateFormat('hh:mm a   (MMM dd)').format(
                                      DateTime.fromMillisecondsSinceEpoch(
                                          int.parse(document['timestamp']))),
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12.0 * wr,
                                      fontStyle: FontStyle.italic),
                                ),
                                margin: EdgeInsets.only(
                                    right: 10.0 * wr,
                                    top: 5.0 * hr,
                                    bottom: 5.0 * hr),
                              )
                            ])
                      : Container()
                ])
              : Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
                  document['type'] == 1
                      // Image
                      ? Container(
                          child: FlatButton(
                            child: Material(
                              child: CachedNetworkImage(
                                placeholder: (context, url) => Container(
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.blue),
                                  ),
                                  width: 200.0 * wr,
                                  height: 200.0 * wr,
                                  padding: EdgeInsets.all(70.0 * wr),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8.0),
                                    ),
                                  ),
                                ),
                                errorWidget: (context, url, error) => Material(
                                  child: Image.asset(
                                    'images/img_not_available.jpeg',
                                    width: 200.0 * wr,
                                    height: 200.0 * wr,
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(8.0),
                                  ),
                                  clipBehavior: Clip.hardEdge,
                                ),
                                imageUrl: document['content'],
                                width: 200.0 * wr,
                                height: 200.0 * wr,
                                fit: BoxFit.cover,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                              clipBehavior: Clip.hardEdge,
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          FullPhoto(url: document['content'])));
                            },
                            padding: EdgeInsets.all(0),
                          ),
                          margin: EdgeInsets.only(
                              bottom:
                                  isLastMessageRight(index) ? 0.0 : 10.0 * hr,
                              right: 10.0 * hr),
                        )
                      // Sticker
                      : Container(
                          child: new Image.asset(
                            'images/${document['content']}.gif',
                            width: 100.0 * wr,
                            height: 100.0 * wr,
                            fit: BoxFit.cover,
                          ),
                          margin: EdgeInsets.only(
                              bottom:
                                  isLastMessageRight(index) ? 00.0 : 10.0 * hr,
                              right: 10.0 * wr),
                        ),
                  isLastMessageRight(index)
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                              Container(
                                child: Text(
                                  DateFormat('hh:mm a   (MMM dd)').format(
                                      DateTime.fromMillisecondsSinceEpoch(
                                          int.parse(document['timestamp']))),
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12.0 * wr,
                                      fontStyle: FontStyle.italic),
                                ),
                                margin: EdgeInsets.only(
                                    right: 10.0 * wr,
                                    top: 5.0 * hr,
                                    bottom: 5.0 * hr),
                              )
                            ])
                      : Container()
                ])
        ],
      );
    } else {
      // Left (peer message)
      return Container(
        child: Column(
          children: <Widget>[
            isFirstMessageLeft(index)
                ? Row(crossAxisAlignment: CrossAxisAlignment.end, children: <
                    Widget>[
                    isLastMessageLeft(index, userID)
                        ? Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              // Container(child:Text("ff")),
                              Container(
                                  child: Material(
                                child: CachedNetworkImage(
                                  placeholder: (context, url) => Container(
                                    child: CircularProgressIndicator(
                                      strokeWidth: 1.0,
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.blue[500]),
                                    ),
                                    width: 35.0 * wr,
                                    height: 35.0 * wr,
                                    padding: EdgeInsets.all(10.0 * wr),
                                  ),
                                  imageUrl: url,
                                  width: 35.0 * wr,
                                  height: 35.0 * wr,
                                  fit: BoxFit.cover,
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(18.0),
                                ),
                                clipBehavior: Clip.hardEdge,
                              ))
                            ],
                          )
                        : Container(width: 35.0 * wr),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(width: 10 * wr),
                              Text(userID,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(color: Colors.grey[600])),
                            ]),
                        SizedBox(height: 5 * hr),
                        document['type'] == 0
                            ? Container(
                                child: Text(
                                  document['content'],
                                  style: TextStyle(color: Colors.white),
                                ),
                                padding: EdgeInsets.fromLTRB(
                                    15.0 * wr, 10.0 * hr, 15.0 * wr, 10.0 * hr),
                                width: 200.0 * wr,
                                decoration: BoxDecoration(
                                    color: Colors.blue[500],
                                    borderRadius: BorderRadius.circular(8.0)),
                                margin: EdgeInsets.only(left: 10.0 * wr),
                              )
                            : document['type'] == 1
                                ? Container(
                                    child: FlatButton(
                                      child: Material(
                                        child: CachedNetworkImage(
                                          placeholder: (context, url) =>
                                              Container(
                                            child: CircularProgressIndicator(
                                                // valueColor: AlwaysStoppedAnimation<Color>(
                                                //     themeColor),
                                                ),
                                            width: 200.0 * wr,
                                            height: 200.0 * wr,
                                            padding: EdgeInsets.all(70.0 * wr),
                                            decoration: BoxDecoration(
                                              color: Colors.grey,
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(8.0),
                                              ),
                                            ),
                                          ),
                                          errorWidget: (context, url, error) =>
                                              Material(
                                            child: Image.asset(
                                              'images/img_not_available.jpeg',
                                              width: 200.0 * wr,
                                              height: 200.0 * wr,
                                              fit: BoxFit.cover,
                                            ),
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(8.0),
                                            ),
                                            clipBehavior: Clip.hardEdge,
                                          ),
                                          imageUrl: document['content'],
                                          width: 200.0 * wr,
                                          height: 200.0 * wr,
                                          fit: BoxFit.cover,
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8.0)),
                                        clipBehavior: Clip.hardEdge,
                                      ),
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => FullPhoto(
                                                    url: document['content'])));
                                      },
                                      padding: EdgeInsets.all(0),
                                    ),
                                    margin: EdgeInsets.only(left: 10.0 * wr),
                                  )
                                : Container(
                                    child: new Image.asset(
                                      'images/${document['content']}.gif',
                                      width: 100.0 * wr,
                                      height: 100.0 * wr,
                                      fit: BoxFit.cover,
                                    ),
                                    margin: EdgeInsets.only(
                                        bottom: isLastMessageRight(index)
                                            ? 20.0 * hr
                                            : 10.0 * hr,
                                        right: 10.0 * wr),
                                  ),
                      ],
                    )
                  ])
                : Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      isLastMessageLeft(index, userID)
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                    child: Material(
                                  child: CachedNetworkImage(
                                    placeholder: (context, url) => Container(
                                      child: CircularProgressIndicator(
                                        strokeWidth: 1.0,
                                        valueColor:
                                            AlwaysStoppedAnimation<Color>(
                                                Colors.blue[500]),
                                      ),
                                      width: 35.0 * wr,
                                      height: 35.0 * wr,
                                      padding: EdgeInsets.all(10.0 * wr),
                                    ),
                                    imageUrl: url,
                                    width: 35.0 * wr,
                                    height: 35.0 * wr,
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(18.0),
                                  ),
                                  clipBehavior: Clip.hardEdge,
                                ))
                              ],
                            )
                          : Container(width: 35.0 * wr),
                      document['type'] == 0
                          ? Container(
                              child: Text(
                                document['content'],
                                style: TextStyle(color: Colors.white),
                              ),
                              padding: EdgeInsets.fromLTRB(
                                  15.0 * wr, 10.0 * hr, 15.0 * wr, 10.0 * hr),
                              width: 200.0 * wr,
                              decoration: BoxDecoration(
                                  color: Colors.blue[500],
                                  borderRadius: BorderRadius.circular(8.0)),
                              margin: EdgeInsets.only(left: 10.0 * wr),
                            )
                          : document['type'] == 1
                              ? Container(
                                  child: FlatButton(
                                    child: Material(
                                      child: CachedNetworkImage(
                                        placeholder: (context, url) =>
                                            Container(
                                          child: CircularProgressIndicator(
                                              // valueColor: AlwaysStoppedAnimation<Color>(
                                              //     themeColor),
                                              ),
                                          width: 200.0 * wr,
                                          height: 200.0 * wr,
                                          padding: EdgeInsets.all(70.0 * wr),
                                          decoration: BoxDecoration(
                                            color: Colors.grey,
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(8.0),
                                            ),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Material(
                                          child: Image.asset(
                                            'images/img_not_available.jpeg',
                                            width: 200.0 * wr,
                                            height: 200.0 * wr,
                                            fit: BoxFit.cover,
                                          ),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(8.0),
                                          ),
                                          clipBehavior: Clip.hardEdge,
                                        ),
                                        imageUrl: document['content'],
                                        width: 200.0 * wr,
                                        height: 200.0 * wr,
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.0)),
                                      clipBehavior: Clip.hardEdge,
                                    ),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => FullPhoto(
                                                  url: document['content'])));
                                    },
                                    padding: EdgeInsets.all(0),
                                  ),
                                  margin: EdgeInsets.only(left: 10.0 * wr),
                                )
                              : Container(
                                  child: new Image.asset(
                                    'images/${document['content']}.gif',
                                    width: 100.0 * wr,
                                    height: 100.0 * wr,
                                    fit: BoxFit.cover,
                                  ),
                                  margin: EdgeInsets.only(
                                      bottom: isLastMessageRight(index)
                                          ? 20.0 * hr
                                          : 10.0 * hr,
                                      right: 10.0 * wr),
                                ),
                    ],
                  ),

            // Time
            isLastMessageLeft(index, userID)
                ? Container(
                    child: Text(
                      DateFormat('hh:mm a   (MMM dd)').format(
                          DateTime.fromMillisecondsSinceEpoch(
                              int.parse(document['timestamp']))),
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12.0 * wr,
                          fontStyle: FontStyle.italic),
                    ),
                    margin: EdgeInsets.only(
                        left: 50.0 * wr, top: 5.0 * hr, bottom: 5.0 * hr),
                  )
                : Container()
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
        margin: EdgeInsets.only(bottom: 10.0 * hr),
      );
    }
  }

  bool isLastMessageLeft(int index, userID) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[index - 1]['idFrom'] != listMessage[index]['idFrom']) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isFirstMessageLeft(int index) {
    if (index == listMessage.length - 1 ||
        (index < listMessage.length - 1 &&
            ((listMessage[index]['idFrom'] !=
                listMessage[index + 1]['idFrom'])))) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index) {
    if ((index > 0 &&
            listMessage != null &&
            listMessage[index - 1]['idFrom'] != fbu.uid) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> onBackPress() {
    if (isShowSticker) {
      setState(() {
        isShowSticker = false;
      });
    } else {
      Navigator.pop(context);
    }

    return Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              // List of messages
              buildListMessage(),

              // Sticker
              (isShowSticker ? buildSticker() : Container()),

              // Input content
              buildInput(),
            ],
          ),

          // Loading
          buildLoading()
        ],
      ),
      onWillPop: onBackPress,
    );
  }

  Widget buildSticker() {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => onSendMessage('mimi1', 2),
                child: new Image.asset(
                  'images/mimi1.gif',
                  width: 50.0 * wr,
                  height: 50.0 * wr,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi2', 2),
                child: new Image.asset(
                  'images/mimi2.gif',
                  width: 50.0 * wr,
                  height: 50.0 * wr,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi3', 2),
                child: new Image.asset(
                  'images/mimi3.gif',
                  width: 50.0 * wr,
                  height: 50.0 * wr,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => onSendMessage('mimi4', 2),
                child: new Image.asset(
                  'images/mimi4.gif',
                  width: 50.0 * wr,
                  height: 50.0 * wr,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi5', 2),
                child: new Image.asset(
                  'images/mimi5.gif',
                  width: 50.0 * wr,
                  height: 50.0 * wr,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi6', 2),
                child: new Image.asset(
                  'images/mimi6.gif',
                  width: 50.0 * wr,
                  height: 50.0 * wr,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => onSendMessage('mimi7', 2),
                child: new Image.asset(
                  'images/mimi7.gif',
                  width: 50.0 * wr,
                  height: 50.0 * wr,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi8', 2),
                child: new Image.asset(
                  'images/mimi8.gif',
                  width: 50.0 * wr,
                  height: 50.0 * wr,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => onSendMessage('mimi9', 2),
                child: new Image.asset(
                  'images/mimi9.gif',
                  width: 50.0 * wr,
                  height: 50.0 * wr,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
      decoration: new BoxDecoration(
          border:
              new Border(top: new BorderSide(color: Colors.grey, width: 0.5)),
          color: Colors.white),
      padding: EdgeInsets.all(5.0 * wr),
      height: 180.0 * hr,
    );
  }

  Widget buildLoading() {
    return Positioned(
      child: isLoading
          ? Container(
              child: Center(child: CircularProgressIndicator()
                  // valueColor: AlwaysStoppedAnimation<Color>(themeColor)),
                  ),
              color: Colors.white.withOpacity(0.8),
            )
          : Container(),
    );
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          // Button send image
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 1.0 * wr),
              child: new IconButton(
                icon: new Icon(Icons.image),
                onPressed: getImage,
                color: Colors.grey[500],
              ),
            ),
            color: Colors.white,
          ),
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 1.0 * wr),
              child: new Container(),
            ),
            color: Colors.white,
          ),

          // Edit text
          Flexible(
            child: Container(
              child: TextField(
                style: TextStyle(color: Colors.black, fontSize: 15.0 * wr),
                controller: textEditingController,
                decoration: InputDecoration.collapsed(
                  hintText: 'Type your message...',
                  hintStyle: TextStyle(color: Colors.grey),
                ),
                focusNode: focusNode,
              ),
            ),
          ),

          // Button send message
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 8.0 * wr),
              child: new IconButton(
                icon: new Icon(Icons.send),
                onPressed: () => onSendMessage(textEditingController.text, 0),
                color: Colors.blue[500],
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 50.0 * hr,
      decoration: new BoxDecoration(
          border: new Border(
              top: new BorderSide(color: Colors.grey, width: 0.5 * wr)),
          color: Colors.white),
    );
  }

  Widget buildListMessage() {
    return Flexible(
      child: messagesID == ''
          ? Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.green)))
          : StreamBuilder(
              stream: Firestore.instance
                  .collection('messages')
                  .document(messagesID)
                  .collection(messagesID)
                  .orderBy('timestamp', descending: true)
                  .limit(20)
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(child: CircularProgressIndicator());
                  // valueColor:
                  //     AlwaysStoppedAnimation<Color>(themeColor)));
                } else {
                  listMessage = snapshot.data.documents;
                  return ListView.builder(
                    padding: EdgeInsets.only(
                        top: 10.0 * hr, left: 10 * wr, right: 10 * hr),
                    itemBuilder: (context, index) =>
                        buildItem(index, snapshot.data.documents[index]),
                    itemCount: snapshot.data.documents.length,
                    reverse: true,
                    controller: listScrollController,
                  );
                }
              },
            ),
    );
  }
}
