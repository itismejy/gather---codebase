import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'colors.dart' as app_colors;
import 'text_styles.dart' as text_styles;
import 'classes.dart';
import 'firebase_backend.dart' as fbe;

import 'components.dart';
import 'edit_info.dart';
import 'further_info.dart';

void main() => runApp(PastScreen());

class PastScreen extends StatefulWidget {
  @override
  State<PastScreen> createState() {
    return _PastScreenState();
  }
}

class _PastScreenState extends State<PastScreen> {
  List<Event> events;

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: app_colors.SWATCH_B,
        appBar: topBar(true, context, false),
        body: Row(
          children: <Widget>[
            FutureBuilder<List<Event>>(
                future: fbe.getPastEvents(),
                builder: (BuildContext context,
                    AsyncSnapshot<List<Event>> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return Text("Failed to retrieve feed");
                    case ConnectionState.active:
                    case ConnectionState.waiting:
                      return SpinKitWanderingCubes(color: Colors.green);
                    case ConnectionState.done:
                      if (snapshot.hasError) {
                        return Text("Error: ${snapshot.error}");
                      }
                      return FeedBody(events: snapshot.data);
                  }
                  return null;
                })
          ],
          mainAxisAlignment: MainAxisAlignment.center,
        ));
  }
}

class FeedBody extends StatelessWidget {
  final List<Event> events;

  FeedBody({this.events});

  @override
  Widget build(BuildContext context) {
    var eventList = this
        .events
        .map((event) => EventBadge(event: event))
        .cast<Widget>()
        .toList();
    return Expanded(
        child: Stack(children: <Widget>[
      ListView(children: eventList),
    ]));
  }
}

class MeetingSummary extends StatelessWidget {
  final String description;
  final DateTime date;
  final AttendanceState attendanceState;
  final String host;

  MeetingSummary(
      {Key key,
      @required this.description,
      @required this.date,
      @required this.attendanceState,
      @required this.host})
      : super(key: key);

// Event Title stuff (font size, color)
// Eventually make text color match going/undecided/busy color
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
            child: Text(
          this.description,
          style: text_styles.small,
          textAlign: TextAlign.center,
        )),
        Padding(
            padding: EdgeInsets.only(right: 15 * wr),
            child: RibbonDate(
              date: date,
              attendanceState: attendanceState,
              host: host,
            )),
      ],
    );
  }
}

class _RibbonPainter extends CustomPainter {
  final double depth;
  final Color color;

  _RibbonPainter({this.depth, this.color}) : super();

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawPath(
        Path()
          ..addPolygon([
            Offset(0, 0),
            Offset(0, size.height + depth),
            Offset(size.width / 2, size.height),
            Offset(size.width, size.height + depth),
            Offset(size.width, 0)
          ], true),
        Paint()..color = color);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class RibbonDate extends StatelessWidget {
  final DateTime date;
  final AttendanceState attendanceState;
  final String host;

  RibbonDate(
      {Key key,
      @required this.date,
      @required this.attendanceState,
      @required this.host})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var ribbonColor = Colors.black;
    var textColor = app_colors.TEXT_PRIMARY;
    switch (attendanceState) {
      case AttendanceState.BUSY:
        ribbonColor = app_colors.BUSY_BANNER;
        textColor = app_colors.HEADER;
        break;
      case AttendanceState.GOING:
        ribbonColor = app_colors.GOING_BANNER;
        if (host == myUsername) {
          ribbonColor = app_colors.HOST_BANNER;
        }
        textColor = app_colors.HEADER;
        break;
      case AttendanceState.UNDECIDED:
        ribbonColor = app_colors.UNDECIDED_BANNER;
        textColor = app_colors.HEADER;
        break;
    }
    return CustomPaint(
        painter: _RibbonPainter(depth: 20, color: ribbonColor),
        child: SizedBox(
            width: 50 * wr,
            child: Column(
              children: <Widget>[
                Text(
                  DateFormat.MMM().format(date).toUpperCase(),
                  style: text_styles.withColor(textColor, text_styles.small),
                ),
                Text(date.day.toString(),
                    style: text_styles.withColor(textColor, text_styles.small)),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            )));
  }
}

// Profile Icons for who's going (I think) -Grayson
class _Profile extends StatelessWidget {
  final User person;
  final double size;

  _Profile({Key key, @required this.person, @required this.size});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.size,
      height: this.size,
      child: CircleAvatar(
          radius: this.size + 10 * wr, backgroundImage: person.userImage),
      margin: EdgeInsets.symmetric(horizontal: 2.5 * hr),
    );
    // return Container(
    //   height: this.size,
    //   width: this.size,
    //   decoration: BoxDecoration(
    //       image: DecorationImage(fit: BoxFit.fill, image: person.userImage)),
    //   margin: EdgeInsets.symmetric(horizontal: 2.5 * hr),
    // );
  }
}

class MeetingDetails extends StatelessWidget {
  final User host;
  final List<User> people;
  final DateTime start;
  final DateTime end;
  final String location;
  final Event event;
  int numGoing = 0;

  MeetingDetails(
      {Key key,
      @required this.host,
      @required this.people,
      @required this.start,
      @required this.end,
      @required this.location,
      @required this.event})
      : super(key: key);

  Widget getProfiles() {
    int numAttendeesGoing = 0;
    if (event.attendees?.isEmpty ?? true) {
    } else {
      List<AttendanceState> stateList = event.attendees.values.toList();
      for (int i = 0; i < stateList.length; i++) {
        if (stateList[i] == AttendanceState.GOING) {
          numAttendeesGoing++;
        }
      }
    }
    numGoing = numAttendeesGoing;
    int j;
    if (numAttendeesGoing >= 3) {
      j = 2;
    } else {
      j = numAttendeesGoing - 1;
    }
    List<User> people1 = people;
    people1.removeWhere((person) => person.userID == host.userID);
    if (j == 0) {
      return Row(
        children: [_Profile(person: host, size: 45)],
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
      );
    }
    return Row(
      children: [_Profile(person: host, size: 45)] +
          people1.sublist(0, j).map((person) {
            return _Profile(person: person, size: 30);
          }).toList(),
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
    );
  }

  @override
  Widget build(BuildContext context) {
    var format = DateFormat.jm();
    var rightSide = <Widget>[
      getProfiles(),
    ];
    if (numGoing > 3) {
      rightSide.add(Text("and ${numGoing - 3} others going",
          style: text_styles.withColor(
              app_colors.TEXT_PRIMARY.withAlpha(75), text_styles.small)));
    }
    return Container(
        child: Row(
      children: <Widget>[
        Expanded(
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Column(
                children: <Widget>[
                  Container(
                      width: 160 * wr,
                      child: Text(
                          format.format(start) + " to " + format.format(end),
                          style: text_styles.medium,
                          textAlign: TextAlign.left)),
                  SizedBox(height: 20 * hr),
                  Container(
                      width: 160 * wr,
                      child: Text(location,
                          style: text_styles.medium, textAlign: TextAlign.left))
                ],
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
              )
            ]),
            flex: 1),
        VerticalLine(
          lineLength: 70 * hr,
          lineWidth: 1 * wr,
        ),
        Expanded(
            child: Column(
          children: rightSide,
          mainAxisAlignment: MainAxisAlignment.center,
        ))
      ],
      mainAxisAlignment: MainAxisAlignment.center,
    ));
  }
}

class MeetingFooter extends StatelessWidget {
  static const INDICATOR_SIZE = 10.0;
  // final String eventType;
  final AttendanceState attending;
  final String host;
// @required this.eventType,
  MeetingFooter({Key key, @required this.attending, @required this.host})
      : super(key: key);

  Widget circle(Color color) => Container(
        margin: EdgeInsets.symmetric(horizontal: 6),
        width: INDICATOR_SIZE,
        height: INDICATOR_SIZE,
        decoration: BoxDecoration(shape: BoxShape.circle, color: color),
      );

  @override
  Widget build(BuildContext context) {
    Color attendanceStateColor = Colors.black;
    Color activityTypeColor = Colors.black;
    String isGoingText = "";
    switch (attending) {
      case AttendanceState.GOING:
        attendanceStateColor = app_colors.GOING_ATTENDANCE;
        activityTypeColor = app_colors.GOING_TYPE;
        if (host == myUsername) {
          isGoingText = "Hosted";
          attendanceStateColor = app_colors.HOST_ATTENDANCE;
          activityTypeColor = app_colors.HOST_TYPE;
        } else {
          isGoingText = "Went";
        }
        // Idk how to change color of event title, if we can tho that would be nice.
        break;
      case AttendanceState.BUSY:
        attendanceStateColor = app_colors.BUSY_ATTENDANCE;
        activityTypeColor = app_colors.BUSY_TYPE;
        isGoingText = "Was Busy";
        break;
      case AttendanceState.UNDECIDED:
        attendanceStateColor = app_colors.UNDECIDED_ATTENDANCE;
        activityTypeColor = app_colors.UNDECIDED_TYPE;
        isGoingText = "Was Undecided";
        break;
    }
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            circle(activityTypeColor),
            // Text(eventType,
            //     style: text_styles.withColor(
            //         activityTypeColor, text_styles.SMALL)),
            Spacer(),
            Text(isGoingText,
                style: text_styles.withColor(
                    attendanceStateColor, text_styles.small)),
            circle(attendanceStateColor)
          ],
          crossAxisAlignment: CrossAxisAlignment.center,
        )
      ],
      mainAxisAlignment: MainAxisAlignment.center,
    );
  }
}

class _VerticalLinePainter extends CustomPainter {
  final double lineWidth;
  final double lineLength;

  _VerticalLinePainter({this.lineLength, this.lineWidth});

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawLine(
        Offset(0, 0),
        Offset(0, this.lineLength),
        Paint()
          ..color = Colors.black
          ..strokeWidth = this.lineWidth);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class VerticalLine extends StatelessWidget {
  final double lineWidth;
  final double lineLength;

  VerticalLine({Key key, this.lineLength, this.lineWidth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: lineLength,
        child: CustomPaint(
            painter: _VerticalLinePainter(
                lineLength: lineLength, lineWidth: lineWidth)));
  }
}

class EventBadge extends StatefulWidget {
  final Event event;
  EventBadge({Key key, @required this.event}) : super(key: key);
  @override
  State<EventBadge> createState() {
    return _EventBadgeState(event: event);
  }
}

class _EventBadgeState extends State<EventBadge> {
  Event event;
  _EventBadgeState({this.event});
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var cardColor = app_colors.HEADER;
    var backgroundColor = Colors.black;
    switch (event.attendanceState) {
      case AttendanceState.BUSY:
        cardColor = app_colors
            .BUSY_HEADER; // Headers are white regardless atm, might change later though -Grayson
        backgroundColor = app_colors.BUSY_BG;
        break;
      case AttendanceState.UNDECIDED:
        backgroundColor = app_colors.UNDECIDED_BG;
        // Color.lerp(app_colors.UNDECIDED_GRAY, app_colors.BOX_SECONDARY, 0.75);
        // We changed the Undecided background to a standard color for now
        break;
      case AttendanceState.GOING:
        backgroundColor = app_colors.GOING_BG;
        if (event.host.userName == myUsername) {
          backgroundColor = app_colors.HOST_BG;
        }
        break;
    }
    double initial = 0;
    double distance = 0;
    return GestureDetector(
        onPanStart: (DragStartDetails details) {
          initial = details.globalPosition.dx;
        },
        onPanUpdate: (DragUpdateDetails details) {
          distance = details.globalPosition.dx - initial;
        },
        onPanEnd: (DragEndDetails details) async {
          //+ve distance signifies a drag from left to right(start to end)
          //-ve distance signifies a drag from right to left(end to start)
          if (event.hostID != fbu.uid) {
            if (distance > 0) {
              if (event.attendanceState == AttendanceState.UNDECIDED) {
                fbe.changeStatus(event.id, "busy");
                event.attendanceState = AttendanceState.BUSY;
                setState(() {});
              } else if (event.attendanceState == AttendanceState.GOING) {
                fbe.changeStatus(event.id, "undecided");
                event.attendanceState = AttendanceState.UNDECIDED;
                setState(() {});
              }
            } else {
              if (event.attendanceState == AttendanceState.UNDECIDED) {
                fbe.changeStatus(event.id, "going");
                event.attendanceState = AttendanceState.GOING;
                setState(() {});
              } else if (event.attendanceState == AttendanceState.BUSY) {
                fbe.changeStatus(event.id, "undecided");
                event.attendanceState = AttendanceState.UNDECIDED;
                setState(() {});
              }
            }
          }
        },
        child: FlatButton(
            padding: EdgeInsets.symmetric(
                horizontal: 3), // PADDING, adjust this to change card width
            child: Container(
              height: 200 * hr,
              width: size.width,
              margin: EdgeInsets.only(bottom: 4 * hr),
              padding: EdgeInsets.zero,
              child: Card(
                  elevation: 5,
                  color: backgroundColor,
                  clipBehavior: Clip.hardEdge,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  child: Column(children: <Widget>[
                    Container(
                        height: 50 * hr,
                        color: cardColor,
                        child: MeetingSummary(
                          date: event.start,
                          description: event.title,
                          attendanceState: event.attendanceState,
                          host: event.host.userName,
                        )),
                    SizedBox(
                        height: 120 * hr,
                        child: MeetingDetails(
                          people: event.attendees.keys.toList(),
                          host: event.host,
                          start: event.start,
                          end: event.end,
                          location: event.location,
                          event: event,
                        )),
                    Expanded(
                        child: Container(
                            color: cardColor,
                            child: MeetingFooter(
                              // eventType: event.activityType,
                              attending: event.attendanceState,
                              host: event.host.userName,
                            ))),
                  ])),
            ),
            onPressed: () {
              Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) =>
                        new FurtherInfo(thisID: this.event.id, isPast: true)),
              ).then((value) {});
            }));
  }
}
