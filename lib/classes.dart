import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

String myUsername;
ImageProvider<dynamic> myUserImage;
String myUserImageURL;
String myUserEmail;
Map<User, AttendanceState> map = new Map<User, AttendanceState>();
FirebaseUser fbu;
Profile p;
User current =
    new User(userID: fbu.uid, userImage: myUserImage, userName: myUsername);

void mapping(User user, AttendanceState state, Map<User, AttendanceState> map) {
  List<MapEntry<User, AttendanceState>> users =
      new List<MapEntry<User, AttendanceState>>();
  users.add(new MapEntry<User, AttendanceState>(user, state));
  map.addEntries(users);
}

List<User> listUserFromJson(List<dynamic> userList) {
  List<User> list = userList.map((j) => User.fromJson(j)).cast<User>().toList();
  return list;
}

class User {
  String userName;
  String userID;
  ImageProvider<dynamic> userImage;
  User(
      {@required this.userName,
      @required this.userImage,
      @required this.userID});
  // User(this.userName,this.userImage);
  // clicked is used in the search and add friends page to check or uncheck a box
  bool clicked = true;
  String toString() {
    return this.userName;
  }

  bool operator ==(o) => o is User && o.userID == userID;
  int get hashCode => (userID.hashCode);

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        userName: json['username'],
        userImage: new NetworkImage(json['image']),
        userID: "");
  }
}

enum AttendanceState { GOING, BUSY, UNDECIDED }

String toStringAtt(AttendanceState state) {
  if (state == AttendanceState.GOING) {
    return "going";
  } else if (state == AttendanceState.BUSY) {
    return "busy";
  } else {
    return "going";
  }
}

AttendanceState toAttendanceState(String attendanceString) {
  switch (attendanceString) {
    case "going":
      return AttendanceState.GOING;
    case "busy":
      return AttendanceState.BUSY;
    case "undecided":
      return AttendanceState.UNDECIDED;
    default:
      return null;
  }
}

class Event {
  String id;
  String title;
  DateTime start;
  DateTime end;
  User host;
  String hostID;
  String messagesID;
  Map<User, AttendanceState> attendees;
  // final String activityType;
  AttendanceState attendanceState;
  String location;

  Event(
      {this.id,
      @required this.title,
      @required this.start,
      @required this.end,
      @required this.host,
      @required this.attendees,
      // @required this.activityType,
      @required this.attendanceState,
      @required this.location,
      @required this.hostID,
      @required this.messagesID});
  AttendanceState getAttendanceState() {
    return this.attendanceState;
  }

  String toString() {
    return "  Host:" +
        this.host.userName +
        "is hosting at" +
        this.location +
        " from " +
        start.toString() +
        " to " +
        end.toString();
  }

  factory Event.fromJson(Map<String, dynamic> json, User user) {
    AttendanceState myStatus;
    Map<User, AttendanceState> attendees = {};
    // Uint8List friendImage;
    // void setVariables(String imageURL) async {
    //   friendImage = await getImage(myUserImageURL);
    // }

    var attenderList = json['Attenders'];
    var eventData = json['Events'];
    for (int i = 0; i < attenderList.length; i++) {
      Map<String, dynamic> thisUser = attenderList[i];
      // setVariables(thisUser['user']['image']);
      // myUserImage = MemoryImage(Uint8image);
      User us = User(
          userName: thisUser['user']['username'],
          userImage: NetworkImage(thisUser['user']['image']),
          userID: "");
      AttendanceState status = toAttendanceState(thisUser['status']);
      attendees[us] = status;
      if (user.userName == us.userName) {
        myStatus = status;
      }
    }
    return Event(
        messagesID: "2",
        id: eventData['id'].toString(),
        title: eventData['name'],
        start: DateTime.parse(eventData['start']),
        end: DateTime.parse(eventData['end']),
        host: User(
            userName: eventData['user']['username'],
            userImage: NetworkImage(eventData['user']['image']),
            userID: ""),
        hostID: "",
        attendees: attendees,
        attendanceState: myStatus,
        location: eventData['location']);
  }
}

class Profile {
  String myName;
  String imageSource;
  List<User> friendnames;
  int incoming;
  Profile(String inputName, String inputImage, List<User> inputUsers, int inputIncoming) {
    myName = inputName;
    imageSource = inputImage;
    friendnames = inputUsers;
    incoming = inputIncoming;
  }
  String toString() {
    return myName;
  }
}

class Response {
  String username;
  bool response;
}

class Message {
  String text;
  User user;
  Message({@required this.text, @required this.user});
  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
        text: json['text'],
        user: User(
            userName: json['sender']['username'],
            userImage: NetworkImage(json['sender']['image']),
            userID: ""));
  }
  @override
  String toString() {
    return this.user.userName + " " + this.text;
  }
}
