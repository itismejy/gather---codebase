import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:grouper/components.dart';
import 'colors.dart' as app_colors;
import 'components.dart';
import 'classes.dart';
import 'search_friends.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'firebase_backend.dart' as fbe;

void main() => runApp(EditInfo());

Event newEvent;
void mapping(User user, AttendanceState state, Map<User, AttendanceState> map) {
  List<MapEntry<User, AttendanceState>> users =
      new List<MapEntry<User, AttendanceState>>();
  users.add(new MapEntry<User, AttendanceState>(user, state));
  map.addEntries(users);
}

class EditInfo extends StatelessWidget {
  final Event newEvent;
  final bool isNew;
  EditInfo({Key key, this.newEvent, this.isNew}) : super(key: key);
  Widget build(BuildContext context) {
    return EditState(newEvent: newEvent);
  }
}

class EditState extends StatefulWidget {
  final User host;
  final Event newEvent;
  EditState({Key key, this.host, this.newEvent}) : super(key: key);
  State<StatefulWidget> createState() {
    return EditInfoPage(host: host, newEvent: newEvent);
  }
}

class EventActionButton extends StatelessWidget {
  final String text;
  final void Function() onPressed;
  final double width;

  EventActionButton({Key key, this.text, this.onPressed, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50 * hr,
        width: this.width,
        margin: EdgeInsets.all(10 * wr),
        padding: EdgeInsets.zero,
        child: RaisedButton(
            elevation: 4,
            color: app_colors.HEADER,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            onPressed: onPressed,
            child: Row(
              children: <Widget>[Text(text)],
              mainAxisSize: MainAxisSize.min,
            )));
  }
}

class EventTitleHeader extends StatelessWidget {
  final TextEditingController titleController;
  final String hintText;

  EventTitleHeader({Key key, this.titleController, this.hintText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Icon(Icons.title),
      Expanded(
          child: TextField(
              controller: titleController,
              decoration: InputDecoration(hintText: hintText))),
    ]);
  }
}

class DatePickerView extends StatelessWidget {
  final IconData iconData;
  final String formattedTime;

  DatePickerView(
      {Key key, @required this.formattedTime, @required this.iconData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50 * hr,
      child: Row(
        children: <Widget>[
          Icon(
            iconData,
            size: 18.0 * wr,
            color: Colors.teal,
          ),
          SizedBox(width: 5 * wr),
          Text(
            formattedTime,
            style: TextStyle(
                color: Colors.teal,
                fontWeight: FontWeight.bold,
                fontSize: 18.0 * wr),
          ),
        ],
      ),
    );
  }
}

class EditInfoDatePicker extends StatelessWidget {
  final DateTime currentTime;
  final void Function(DateTime dateTime) onSet;

  EditInfoDatePicker(
      {Key key, @required this.currentTime, @required this.onSet})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 4.0,
      onPressed: () {
        DatePicker.showPicker(context,
            theme: DatePickerTheme(
              containerHeight: 210.0 * hr,
            ),
            showTitleActions: true,
            pickerModel: CustomPicker(
              currentTime: currentTime,
            ), onConfirm: (end) {
          onSet(end);
        }, locale: LocaleType.en);
      },
      child: DatePickerView(
        formattedTime: parseDateTime(currentTime),
        iconData: Icons.access_time,
      ),
      color: Colors.white,
    );
  }
}

class EditInfoPage extends State<EditState> {
  User host;
  Event newEvent;
  bool existing = false;

  List<String> list;
  List<User> invited;
  List<User> unchangedInvited = [];
  Map<User, AttendanceState> map;
  TextEditingController titleController = new TextEditingController();
  TextEditingController locationController = new TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  EditInfoPage({this.host, this.newEvent});

  _navigateAndDisplay(BuildContext context) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    var result = await Navigator.push(context, MaterialPageRoute(
      builder: (context) {
        return SearchFriends(event: newEvent);
      },
    ));
    setState(() {
      invited = result;
    });
  }

  DateTime dd = DateTime.now();
  DateTime ss = DateTime.now();
  DateTime ee = DateTime.now();

  @override
  initState() {
    super.initState();

    invited = [];
    if (newEvent != null) {
      list = editInfoInitDayTime(newEvent.start, newEvent.end);
      existing = true;
      dd = newEvent.start;
      ss = newEvent.start;
      ee = newEvent.end;
      titleController.text = newEvent.title;
      locationController.text = newEvent.location;
      invited = newEvent.attendees.keys.toList();
      unchangedInvited = newEvent.attendees.keys.toList();
    }
    map = new Map<User, AttendanceState>();

    for (int i = 0; i < invited.length; i++) {
      if (invited[i].userID != fbu.uid) {
        mapping(invited[i], AttendanceState.UNDECIDED, map);
      }
    }
    mapping(User(userName: myUsername, userImage: myUserImage, userID: fbu.uid),
        AttendanceState.GOING, map);
    host = User(userName: myUsername, userImage: myUserImage, userID: fbu.uid);
  }

  void finishEditingEvent() async {
    String s;
    DateTime end = getDateTimeT(dd, ee);
    if (getDateTimeT(dd, ee).compareTo(getDateTimeT(dd, ss)) < 0) {
      end = end.add(Duration(days: 1));
    }
    print(end);
    if (existing) {
      newEvent.title = titleController.text;
      newEvent.start = getDateTimeT(dd, ss);
      newEvent.end = end;
      newEvent.host = host;
      newEvent.hostID = fbu.uid;
      newEvent.attendees = map;
      newEvent.attendanceState = AttendanceState.GOING;
      newEvent.location = locationController.text;
      s = await fbe.editEvent(newEvent);
    } else {
      newEvent = Event(
          messagesID: "",
          title: titleController.text,
          start: getDateTimeT(dd, ss),
          end: end,
          host: host,
          hostID: fbu.uid,
          attendees: map,
          attendanceState: AttendanceState.GOING,
          location: locationController.text);
      s = await fbe.createEvent(newEvent);
    }
    if (invited != null) {
      List<String> invID = [];
      for (int i = 0; i < unchangedInvited.length; i++) {
        invID.add(unchangedInvited[i].userID);
      }

      for (int i = 0; i < invited.length; i++) {
        fbe.invite(s, invited[i].userID, invID);
      }
    }
    Navigator.of(context).pushNamedAndRemoveUntil(
        '/feedscreen', (Route<dynamic> route) => false);
  }

  void _showToast(BuildContext context, String text) {
    final scaffold = _scaffoldKey.currentState;
    scaffold.showSnackBar(
      SnackBar(
        content: Text(
          text,
          style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.bold,
              fontSize: 12 * wr),
        ),
        backgroundColor: app_colors.SWATCH_B,
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var actionButtonWidth = (size.width - 50) * wr;
    return Scaffold(
        key: _scaffoldKey,
        appBar: topBar(true, context, false),
        backgroundColor: app_colors.BACKGROUND,
        body: SingleChildScrollView(
            child: Column(children: <Widget>[
          // Main Card Details
          Card(
            color: app_colors.SWATCH_B,
            clipBehavior: Clip.hardEdge,
            elevation: 4,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(children: [
                  Padding(
                    padding: EdgeInsets.all(10 * wr),
                    child: EventTitleHeader(
                      titleController: titleController,
                      hintText: "Event Title",
                    ),
                  ),
                  // Event Details Box
                  Container(
                      padding: EdgeInsets.all(10 * wr),
                      // color: Colors.white.withAlpha(150),
                      child: Column(
                        children: <Widget>[
                          RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            elevation: 4.0,
                            onPressed: () {
                              DatePicker.showDatePicker(context,
                                  theme: DatePickerTheme(
                                    containerHeight: 210.0,
                                  ),
                                  showTitleActions: true,
                                  minTime: DateTime.now(),
                                  maxTime:
                                      DateTime.now().add(Duration(days: 365)),
                                  onConfirm: (date) {
                                setState(() => dd = date);
                              }, currentTime: dd, locale: LocaleType.en);
                            },
                            child: DatePickerView(
                                formattedTime:
                                    "${dd.year} - ${dd.month} - ${dd.day}",
                                iconData: Icons.date_range),
                            color: Colors.white,
                          ),
                          SizedBox(height: 15 * hr),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                EditInfoDatePicker(
                                    currentTime: ss,
                                    onSet: (newDate) =>
                                        setState(() => ss = newDate)),
                                EditInfoDatePicker(
                                    currentTime: ee,
                                    onSet: (newDate) =>
                                        setState(() => ee = newDate)),
                              ]),
                          SizedBox(height: 5 * hr),
                          Row(children: [
                            Icon(Icons.place),
                            Expanded(
                                child: TextField(
                                    controller: locationController,
                                    decoration:
                                        InputDecoration(hintText: "Address"))),
                          ]),
                        ],
                      )),
                  // Invite Friends Button
                  EventActionButton(
                      text: "+ Invite Friends",
                      onPressed: () => _navigateAndDisplay(context),
                      width: actionButtonWidth),
                  // Delete Event Button
                  EventActionButton(
                      text: "Delete Event",
                      onPressed: () async {
                        if (existing) {
                          await fbe.deleteEvent(newEvent.id, false);
                        }
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/feedscreen', (Route<dynamic> route) => false);
                      },
                      width: actionButtonWidth),
                ])),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.end, children: [
            Container(
                height: 40 * hr,
                width: 150 * wr,
                margin: EdgeInsets.only(
                  top: 50 * hr,
                  right: 5 * wr,
                ),
                padding: EdgeInsets.zero,
                child: RaisedButton(
                    elevation: 4,
                    color: app_colors.HEADER,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(24))),
                    onPressed: () {
                      if (titleController.text == "") {
                        _showToast(
                            context, '"Event Title" cannot be left empty!');
                      } else if (locationController.text == "") {
                        _showToast(context, '"Address" cannot be left empty!');
                      }
                      // else if (getDateTimeT(dd, ss)
                      //         .compareTo(getDateTimeT(dd, ee)) >
                      //     0) {
                      //   _showToast(
                      //       context, 'Event cannot end before it starts!');
                      // }
                      else {
                        finishEditingEvent();
                      }
                    },
                    child: Row(
                      children: <Widget>[Text("Save")],
                      mainAxisSize: MainAxisSize.min,
                    )))
          ]),
          SizedBox(height: 10 * hr),
        ])));
  }
}
