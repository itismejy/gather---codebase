import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'colors.dart' as app_colors;
import 'components.dart';
import 'classes.dart';
import 'firebase_backend.dart' as fbe;

void main() => runApp(SearchFriends());

class SearchFriends extends StatelessWidget {
  Event event;
  SearchFriends({Key key, this.event}) : super(key: key);
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: app_colors.SWATCH_B,
        appBar: topBar(true, context, false),
        body: SFP(event: event));
  }
}

class SFP extends StatefulWidget {
  final List<User> friends;
  final Event event;
  SFP({Key key, this.friends, this.event}) : super(key: key);
  State<StatefulWidget> createState() {
    return SearchFriendsPage(event: event);
  }
}

class SearchFriendsPage extends State<SFP> {
  String friendSearch = "";
  List<User> friends;
  bool isReady = false;
  List<User> invitedFriends = [];
  Event event;
  SearchFriendsPage({this.event});

  initState() {
    super.initState();
    setVariables();
  }

  void setVariables() async {
    Profile p = await fbe.getProfile();
    if (event != null) {
      Map<User, AttendanceState> map = event.attendees;
      List<User> keys = map.keys.toList();
      friends = p.friendnames;
      isReady = true;
      for (int i = 0; i < keys.length; i++) {
        for (int j = 0; j < friends.length; j++) {
          if (keys[i].userID == friends[j].userID) {
            toggleFriendClicked(friends[j]);
          }
        }
      }
    }
    friends = p.friendnames;
    isReady = true;
    setState(() {});
  }

  List<User> queryName(List<User> friends) =>
      friends.where((name) => name.userName.startsWith(friendSearch)).toList();

  void toggleFriendClicked(User user) {
    if (user.clicked == true) {
      user.clicked = false;
      invitedFriends.add(user);
    } else {
      user.clicked = true;
      for (int i = 0; i < invitedFriends.length; i++) {
        if (invitedFriends[i].userName == user.userName) {
          invitedFriends.removeAt(i);
        }
      }
    }
    setState(() {
      /////
    });
  }

  Widget renderFriendList(List<User> friends) {
    return ListView.builder(
        itemCount: friends.length,
        itemBuilder: (BuildContext ctxt, int index) {
          //sort here
          User friendProf = friends[index];
          return Column(children: [
            Center(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                  // Profile Picture
                  Padding(
                    padding: EdgeInsets.only(left: 10 * wr),
                    child: Container(
                        width: 40.0 * wr,
                        height: 40.0 * wr,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image: friendProf.userImage))),
                  ),
                  // Person's name and # of mutual friends
                  // idk how to show actual # of mutual friends
                  Padding(
                      padding: EdgeInsets.only(left: 14 * wr),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(friendProf.userName,
                                textAlign: TextAlign.left,
                                textDirection: TextDirection.ltr,
                                style: TextStyle(
                                  fontSize: 20.0 * wr,
                                )),
                            // Text('# Mutual Friends')
                          ])),
                  Spacer(),
                  // Plus Button
                  Padding(
                      padding: EdgeInsets.only(right: 10 * wr),
                      child: Container(
                        alignment: Alignment(100 * wr, 0),
                        width: 30.0 * wr,
                        height: 30.0 * wr,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: app_colors.SWATCH_A,
                        ),
                        child: IconButton(
                            padding: EdgeInsets.zero,
                            icon: Icon(
                                friendProf.clicked ? Icons.add : Icons.check,
                                color: app_colors.SWATCH_B,
                                size: 25 * wr),
                            onPressed: () {
                              toggleFriendClicked(friendProf);
                            }),
                        /*child: Checkbox(
                            value: added,
                            onChanged: (bool value){
                              setState(() {
                               added = value; 
                              });
                            },
                          )*/
                      ))
                ]))
          ,SizedBox(height:10 * hr)]);
        });
  }

  final searchKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    if (!isReady) {
      return SpinKitWanderingCubes(color: Colors.green);
    }
    return Stack(children: <Widget>[
      Column(children: [
        Padding(
            padding: EdgeInsets.only(top: 14 * hr),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          child: TextField(
                              onChanged: (newString) {
                                setState(() {
                                  friendSearch = newString;
                                });
                              },
                              decoration: InputDecoration(
                                hintText: 'Search',
                                counterText: "",
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.fromLTRB(
                                    10.0 * wr, 10.0 * hr, 20.0 * wr, 10.0 * hr),
                              )))
                    ],
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(25.0)),
                      color: Colors.white),
                  height: 30 * hr,
                  width: (pwidth - 20) * wr,
                ),
              ],
            ))
      ]),
      Padding(
          padding: EdgeInsets.only(top: 60 * hr),
          child: renderFriendList(friends)),
      Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
        Spacer(),
        Row(children: <Widget>[
          Spacer(),
          RaisedButton(
            color: app_colors.HEADER,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(24))),
            child: Row(
              children: <Widget>[
                Text(
                  "Done",
                )
              ],
              mainAxisSize: MainAxisSize.min,
            ),
            onPressed: () {
              Navigator.pop(context, invitedFriends);
            },
          ),
          SizedBox(width: 10 * wr)
        ]),
        SizedBox(height: 40 * hr),
      ])
    ]);
  }
}
