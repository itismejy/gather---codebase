import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grouper/add_friends.dart';
import 'package:grouper/edit_profile.dart';
import 'package:grouper/feed_screen.dart';
import 'package:grouper/forgot_password.dart';
import 'package:grouper/local_notifications.dart';
import 'package:grouper/profile.dart';
import 'package:grouper/search_friends.dart';
import 'package:grouper/settings.dart';
import 'package:grouper/sign_up.dart';
import 'package:grouper/strings.dart';
import 'package:grouper/pastEvents.dart';
import 'sign_in.dart';
import 'text_styles.dart' as text_styles;

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  initializeNotificationPlugin();
  runApp(new Main());
}

class Main extends StatefulWidget {
  State<StatefulWidget> createState() {
    return MainPage();
  }
}

class MainPage extends State<Main> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the MainPage widget.
        '/': (context) => SignIn(),
        // When navigating to the "/feedscreen" route, build the FeedScreen widget.
        '/feedscreen': (context) => FeedScreen(),
        '/pastscreen': (context) => PastScreen(),
        '/editprofile': (context) => EditWidget(),
        '/profile': (context) => ProfileWidget(),
        '/settings': (context) => SettingsWidget(),
        '/forgot': (context) => ForgotWidget(),
        // '/furtherinfo': (context) => FurtherInfo(),
        // '/editinfo': (context) => EditInfo(),
        '/search_friends': (context) => SearchFriends(),
        '/add_friends': (context) => AddFriends(),
        '/signin': (context) => SignIn(),
        '/signup': (context) => SignUp()
      },
      title: TITLE,
      theme: ThemeData(fontFamily: text_styles.DEFAULT_FONT_FAMILY),
      // home: Display(),
    );
  }
}