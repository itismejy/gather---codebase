import 'package:flutter/material.dart';

// Color Mode
bool nightMode = false;

// Header
const HEADER = Color(0xffffffff); // Background color of header

// Running Color Scheme
const SWATCH_A = Color(0xff2EA8E6); // Bolder of the two, (for accents?)
const SWATCH_B = Color(0xffE6FFFC);
const SWATCH_C = Color(0xffd5ebfa); // Muter of the two, (for backgrounds?)
// Going Colors
const GOING_A = Color(0xffA7D76A); // Nice Green
const GOING_B = Color(0xffE4FFC2); // Green Background
// Undecided Colors
const UNDECIDED_A = Color(0xff797979); // Gray
const UNDECIDED_B = Color(0xffD1D1D1); // Gray Background
// Busy Colors
const BUSY_A = Color(0xffDE7575); // Nice Red
const BUSY_B = Color(0xffFFD5D5); // Red Background

const HOST_A = Color(0xff0000FF); // Nice Red
const HOST_B = Color(0xff99ccFF); // Red Background

// Text
const TEXT_PRIMARY = Color(0xff000000); // Main text color
const TEXT_INVERT =
    Color(0xffffffff); // White, for use against dark backgrounds
const TEXT_APPNAME = Color(0xff777777); // Grey, Color for app name
// const TEXT_SECONDARY =  TEXT_PRIMARY.withAlpha(50)    (Just do 50% opacity each time)
// const TEXT_HIGHLIGHT = Color(0xff2EA8E6); // Similar to SWATCH_A but adjusted for readability

// Background
const BACKGROUND = Color(0xffEBEBEB); // Background background color
const GRADIENT_A = Color(0xffC4F3F5); // For sign in / sign up pages
const GRADIENT_B = Color(0xffA6E9ED); // For sign in / sign up pages

// Card Colors
const GOING_HEADER = Color(0xffffffff); // Top and bottom bars
const GOING_ATTENDANCE = GOING_A; // Attendance status (Bottom right)
const GOING_BANNER = GOING_A; // Banner Background
const GOING_BG = GOING_B; // Background (Center)
const GOING_TYPE = GOING_A; // Event Type Text (Bottom left)
const GOING_TOP = GOING_A; // Header Text (Top)

const UNDECIDED_HEADER = Color(0xffffffff);
const UNDECIDED_ATTENDANCE = UNDECIDED_A;
const UNDECIDED_BANNER = UNDECIDED_A;
const UNDECIDED_BG = UNDECIDED_B;
const UNDECIDED_TYPE = UNDECIDED_A;
const UNDECIDED_TOP = UNDECIDED_A;

const BUSY_HEADER = Color(0xffffffff);
const BUSY_ATTENDANCE = BUSY_A;
const BUSY_BANNER = BUSY_A;
const BUSY_BG = BUSY_B;
const BUSY_TYPE = BUSY_A;
const BUSY_TOP = BUSY_A;

const HOST_HEADER = Color(0xffffffff);
const HOST_ATTENDANCE = HOST_A;
const HOST_BANNER = HOST_A;
const HOST_BG = HOST_B;
const HOST_TYPE = HOST_A;
const HOST_TOP = HOST_A;

// Other (idk what these do)
const ICON = Colors.black;
const HOVERING = Color(0xff7D7D7D);

const SIGN_OUT = Color(0xffDE7575); // RED

// Retirees
//const BOX_PRIMARY = Color(0xffB3E6FF);
//const BOX_SECONDARY = Color(0xffffffff);

// const MAYBE_GREEN = Color(0xff99EEBB);
