import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'classes.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:web_socket_channel/io.dart';
import 'package:path_provider/path_provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'local_notifications.dart' as notif;
import 'package:intl/intl.dart';

const HTTP_OK = 200;
const _BACKEND_HOST = "grouper.azurewebsites.net";
String _token;
User _currentUser;
IOWebSocketChannel channel;
FirebaseMessaging _firebaseMessaging;

String getBackendUrl(String apiPath) {
  return "https://$_BACKEND_HOST/$apiPath";
}

void subscribeEvent(String eventId) async {
  channel.sink.add(jsonEncode({'type': 'subscribe', 'id': eventId}));
}

void unsubscribeEvent(String eventId) async {
  channel.sink.add(jsonEncode({'type': 'unsubscribe', 'id': eventId}));
}

Future<int> signIn(String email, String password) async {
  try {
    AuthResult user = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email.trim().toLowerCase(), password: password);
    FirebaseUser addUser = await FirebaseAuth.instance.currentUser();
    _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.requestNotificationPermissions();
    _firebaseMessaging.configure();
    fbu = addUser;
    Firestore.instance
        .collection('users')
        .document(fbu.uid)
        .updateData({'notificationToken': await _firebaseMessaging.getToken()});
    return 1;
    // if (addUser.isEmailVerified) return 1;
    // return 4;
  } catch (e) {
    if (e.message == "The email address is badly formatted") {
      return 2;
    } else {
      return 3;
    }
  }
}

Future<Profile> getProfile() async {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser addUser = await _auth.currentUser();
  var firestore = Firestore.instance;
  DocumentReference docRef =
      firestore.collection('users').document(addUser.uid);
  var data;
  List<User> friendList = [];
  String myName = "";
  DocumentSnapshot datasnapshot = await docRef.get();
  data = datasnapshot.data['friends'];
  myName = datasnapshot.data['username'];
  int incoming = datasnapshot.data['incomingRequests'].length;
  List<String> keys = data.keys.cast<String>().toList();
  for (int i = 0; i < keys.length; i++) {
    DocumentReference friendRef =
        firestore.collection('users').document(keys[i]);
    DocumentSnapshot datasnapshot2 = await friendRef.get();
    if (datasnapshot2.exists) {
      String name = datasnapshot2.data['username'];
      String userID = datasnapshot2.data['user_id'];
      StorageReference imageRef =
          FirebaseStorage.instance.ref().child('profiles/$userID');
      String url = await imageRef.getDownloadURL();
      ImageProvider<dynamic> image = NetworkImage(url);
      User friend = new User(userName: name, userImage: image, userID: userID);
      friendList.add(friend);
    }
  }
  Profile profile = Profile(myName, myUserImageURL, friendList, incoming);
  return profile;
}

Future<User> getCachedCurrentUser() async {
  if (_currentUser != null) {
    return _currentUser;
  }
  Profile prof = await getProfile();
  _currentUser = User(
      userName: prof.myName,
      userImage: NetworkImage(prof.imageSource),
      userID: fbu.uid);
  return _currentUser;
}

Future<String> createEvent(Event event) async {
  var firestore = Firestore.instance;
  DocumentReference eventRef = firestore.collection('events').document();
  DocumentReference userRef = firestore.collection('users').document(fbu.uid);
  userRef.updateData({"eventsID.${eventRef.documentID}": "going"});
  Map<String, String> attendees = {};
  event.attendees.forEach((k, v) async {
    attendees[k.userID] = toStringAtt(v);
  });
  DocumentReference messagesRef = firestore.collection('messages').document();
  messagesRef
      .setData({"{${messagesRef.documentID}": "${messagesRef.documentID}"});

  eventRef.setData({
    'name': event.title,
    'image': "randomString",
    'start': event.start.toString(),
    'end': event.end.toString(),
    'location': event.location,
    'host.id': fbu.uid,
    'id': eventRef.documentID,
    'attendees': attendees,
    'messagesID': messagesRef.documentID
  });
  return eventRef.documentID;
}

Future<void> createBug(String message) async {
  var firestore = Firestore.instance;
  DocumentReference bugRef = firestore.collection('bug').document();
  bugRef.setData({'${fbu.uid}': message});
}

Future<void> editImage(File file) async {
  StorageReference storageReference =
      FirebaseStorage.instance.ref().child('profiles/${fbu.uid}');
  StorageUploadTask uploadTask = storageReference.putFile(file);
  await uploadTask.onComplete;
  String url = await storageReference.getDownloadURL();
  myUserImageURL = url;
  myUserImage = NetworkImage(url);
}

Future<void> editName(String name) async {
  var firestore = Firestore.instance;
  await firestore
      .collection('users')
      .document(fbu.uid)
      .updateData({"username": name});
  myUsername = name;
}

Future<String> editEvent(Event event) async {
  var firestore = Firestore.instance;
  await firestore
      .collection('events')
      .document(event.id)
      .updateData({"name": event.title});
  await firestore
      .collection('events')
      .document(event.id)
      .updateData({"start": event.start.toString()});
  await firestore
      .collection('events')
      .document(event.id)
      .updateData({"end": event.end.toString()});
  await firestore
      .collection('events')
      .document(event.id)
      .updateData({"location": event.location});
  await firestore
      .collection('events')
      .document(event.id)
      .updateData({"attendees": {}});
  await firestore
      .collection('events')
      .document(event.id)
      .updateData({"attendees.${event.hostID}": "going"});
  var keys = event.attendees.keys.toList();
  var values = event.attendees.values.toList();
  for (int i = 0; i < event.attendees.keys.length; i++) {
    var v = keys[i];
    var k = values[i];
    await firestore
        .collection('events')
        .document(event.id)
        .updateData({"attendees.${v.userID}": k.toString()});
  }
  return event.id;
}

Future<Event> getEventDetails(String id, bool isPast) async {
  Firestore _firestore = Firestore.instance;
  DocumentReference userRef = _firestore.collection('users').document(fbu.uid);
  DocumentSnapshot data0 = await userRef.get();
  AttendanceState attendanceState =
      toAttendanceState(data0.data['eventsID']['$id']);
  DocumentReference eventRef = _firestore.collection('events').document(id);
  DocumentSnapshot data1 = await eventRef.get();
  var d1 = data1.data;
  String hostName = "";
  DocumentReference hostRef =
      _firestore.collection('users').document(d1['host.id']);
  DocumentSnapshot snapHost = await hostRef.get();
  hostName = snapHost.data['username'];
  StorageReference imageRef =
      FirebaseStorage.instance.ref().child('profiles/${d1['host.id']}');
  String url = await imageRef.getDownloadURL();
  ImageProvider<dynamic> image = NetworkImage(url);
  Map<User, AttendanceState> attendees = {};
  var data3 = d1['attendees'];
  for (int i = 0; i < data3.keys.length; i++) {
    var keys = data3.keys.toList();
    var v = keys[i];
    DocumentReference attRef = _firestore.collection('users').document(v);
    DocumentSnapshot attD = await attRef.get();
    AttendanceState att;
    if (isPast) {
      att = toAttendanceState(attD.data['pastEventsID'][id]);
    } else {
      att = toAttendanceState(attD.data['eventsID'][id]);
    }
    DocumentReference attendeeRef = _firestore.collection('users').document(v);
    DocumentSnapshot datasnapshot3 = await attendeeRef.get();
    String attendeeName = datasnapshot3.data['username'];
    StorageReference imageRef =
        FirebaseStorage.instance.ref().child('profiles/$v');
    String url = await imageRef.getDownloadURL();
    ImageProvider<dynamic> image = NetworkImage(url);
    User invitedUser = new User(
        userName: attendeeName,
        userImage: image,
        userID: datasnapshot3.data['user_id']);
    attendees[invitedUser] = att;
  }
  Event event = new Event(
      messagesID: d1['messagesID'],
      id: d1['id'],
      title: d1['name'],
      start: DateTime.parse(d1['start']),
      end: DateTime.parse(d1['end']),
      host: User(userName: hostName, userImage: image, userID: d1['host.id']),
      hostID: d1['host.id'],
      attendees: attendees,
      attendanceState: attendanceState,
      location: d1['location']);
  return event;
}

Future<void> removeFriend(String userID) async {
  Firestore firestore = Firestore.instance;
  await firestore
      .collection('users')
      .document(fbu.uid)
      .updateData({"friends.$userID": FieldValue.delete()});
  await firestore
      .collection('users')
      .document(userID)
      .updateData({"friends.${fbu.uid}": FieldValue.delete()});
}

Future<void> sendFriendRequests(String id) async {
  Firestore firestore = Firestore.instance;
  await firestore
      .collection('users')
      .document(fbu.uid)
      .updateData({"outgoingRequests.$id": id});
  await firestore
      .collection('users')
      .document(id)
      .updateData({"incomingRequests.${fbu.uid}": fbu.uid});
}

Future<List<User>> getFriendRequests() async {
  Firestore _firestore = Firestore.instance;
  DocumentReference userRef = _firestore.collection('users').document(fbu.uid);
  DocumentSnapshot data = await userRef.get();
  var keys = data.data['incomingRequests'].keys.toList();
  List<User> users = [];
  for (int i = 0; i < keys.length; i++) {
    DocumentReference incomingRef =
        _firestore.collection('users').document(keys[i]);
    DocumentSnapshot data2 = await incomingRef.get();
    String userId = data2.data['user_id'];
    String username = data2.data['username'];
    StorageReference imageRef =
        FirebaseStorage.instance.ref().child('profiles/$userId');
    String url = await imageRef.getDownloadURL();
    ImageProvider<dynamic> image = NetworkImage(url);
    User u = new User(userName: username, userImage: image, userID: userId);
    users.add(u);
  }
  return users;
}

Future<void> respondFriendRequest(String userID, bool response) async {
  Firestore firestore = Firestore.instance;
  await firestore
      .collection('users')
      .document(fbu.uid)
      .updateData({"incomingRequests.$userID": FieldValue.delete()});
  await firestore
      .collection('users')
      .document(userID)
      .updateData({"outgoingRequests.${fbu.uid}": FieldValue.delete()});
  if (response == true) {
    await firestore
        .collection('users')
        .document(fbu.uid)
        .updateData({"friends.$userID": userID});
    await firestore
        .collection('users')
        .document(userID)
        .updateData({"friends.${fbu.uid}": fbu.uid});
  }
  getProfile();
}

Future<List<User>> getNonFriends() async {
  Firestore _firestore = Firestore.instance;
  QuerySnapshot querySnapshot =
      await Firestore.instance.collection("users").getDocuments();
  List<DocumentSnapshot> list = querySnapshot.documents;
  List<String> users = [];
  List<User> toReturn = [];
  for (int i = 0; i < list.length; i++) {
    users.add(list[i].data['user_id']);
  }
  users.remove(fbu.uid);
  DocumentReference userRef = _firestore.collection('users').document(fbu.uid);
  DocumentSnapshot data = await userRef.get();

  var dd = data.data['outgoingRequests'];
  List<String> keys = dd.keys.cast<String>().toList();
  if (keys.length != 0) {
    for (int i = 0; i < keys.length; i++) {
      users.remove(keys[i]);
    }
  }

  var dd2 = data.data['incomingRequests'];
  List<String> keys1 = dd2.keys.cast<String>().toList();
  if (keys1.length != 0) {
    for (int i = 0; i < keys1.length; i++) {
      users.remove(keys1[i]);
    }
  }
  List<String> keys2 = data.data['friends'].keys.cast<String>().toList();
  if (keys2.length != 0) {
    for (int i = 0; i < keys2.length; i++) {
      users.remove(keys2[i]);
    }
  }
  for (int i = 0; i < users.length; i++) {
    DocumentReference nonFRef =
        _firestore.collection('users').document(users[i]);
    DocumentSnapshot data2 = await nonFRef.get();
    String userId = data2.data['user_id'];
    String username = data2.data['username'];
    StorageReference imageRef =
        FirebaseStorage.instance.ref().child('profiles/$userId');
    String url = await imageRef.getDownloadURL();
    ImageProvider<dynamic> image = NetworkImage(url);
    User u = new User(userName: username, userImage: image, userID: userId);
    toReturn.add(u);
  }
  return toReturn;
}

Future<List<Message>> textInChatroom(String id) async {
  var body = json.encode({'id': id});
  var resp = await http.post(getBackendUrl('event/get_message'),
      body: body,
      headers: {'auth-token': _token, 'Content-Type': 'application/json'});
  if (resp.statusCode == HTTP_OK) {
    var data = jsonDecode(resp.body);
    var rawMessages = data['data'];
    List<Message> list =
        rawMessages.map((j) => Message.fromJson(j)).cast<Message>().toList();
    return list;
  } else {
    return null;
  }
}

Future<void> sendText(String id, String text) async {
  var body = json.encode({'id': id, 'text': text});
  var resp = await http.post(getBackendUrl('event/send_message'),
      body: body,
      headers: {'auth-token': _token, 'Content-Type': 'application/json'});
  if (resp.statusCode == HTTP_OK) {
    return null;
  } else {
    return null;
  }
}

Future<bool> forgotPassword(String email) async {
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  try {
    await _firebaseAuth.sendPasswordResetEmail(email: email);
    return true;
  } catch (e) {
    print(e.message);
    return false;
  }
}

Future<void> invite(String id, String userID, List<String> alrInv) async {
  if (userID == fbu.uid) {
    return null;
  }
  if (alrInv.contains(userID)) {
    return;
  }
  Firestore _firestore = Firestore.instance;
  await _firestore
      .collection('users')
      .document(userID)
      .updateData({"eventsID.$id": "undecided"});
  await _firestore
      .collection('events')
      .document(id)
      .updateData({"attendees.$userID": "undecided"});
}

Future<File> getImageFileFromAssets() async {
  final byteData = await rootBundle.load('assets/blank_profile.jpg');

  final file =
      File('${(await getTemporaryDirectory()).path}/blank_profile.jpg');
  await file.writeAsBytes(byteData.buffer
      .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

  return file;
}

Future<int> createAccount(
    String username, String password, String email) async {
  FirebaseAuth _auth = FirebaseAuth.instance;
  Firestore _firestore = Firestore.instance;
  try {
    AuthResult user = await _auth.createUserWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser addUser = await _auth.currentUser();
    // await addUser.sendEmailVerification();
    DocumentReference userRef =
        _firestore.collection('users').document(addUser.uid);
    userRef.setData({
      'email': email,
      'username': username,
      'user_id': addUser.uid,
      'friends': {},
      'incomingRequests': {},
      'outgoingRequests': {},
      'eventsID': {},
      'pastEventsID': {}
    });
    DocumentReference optionsRef =
        _firestore.collection('searchOptions').document(addUser.uid);
    optionsRef.setData({
      'user_id': addUser.uid,
    });
    // final filePath = await FlutterAbsolutePath.getAbsolutePath(uriString);
    // File file = File(url);
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child('profiles/${addUser.uid}');
    File f = await getImageFileFromAssets();
    StorageUploadTask uploadTask = storageReference.putFile(f);
    await uploadTask.onComplete;
    return 0;
  } catch (e) {
    print(e.message);
    if (e.message == "The email address is badly formatted.") {
      return 1;
    }
    return 2;
  }
}

Future<void> changeStatus(String id, String attendanceState) async {
  Firestore _firestore = Firestore.instance;
  DocumentReference userRef = _firestore.collection('users').document(fbu.uid);
  userRef.updateData({"eventsID.$id": attendanceState.toString()});
  DocumentReference eventRef = _firestore.collection('events').document(id);
  eventRef.updateData({"attendees.${fbu.uid}": attendanceState.toString()});
}

Future<void> deleteEvent(String id, bool isPast) async {
  Firestore _firestore = Firestore.instance;
  DocumentReference eventRef = _firestore.collection('events').document(id);
  DocumentSnapshot d1 = await eventRef.get();
  var keys = d1.data['attendees'].keys.toList();
  try {
    for (int a = 0; a < d1.data['attendees'].length; a++) {
      DocumentReference userRef =
          _firestore.collection('users').document(keys[a]);
      if (isPast) {
        await userRef.updateData({'pastEventsID.$id': FieldValue.delete()});
      } else {
        await userRef.updateData({'eventsID.$id': FieldValue.delete()});
      }
    }
    await Firestore.instance.runTransaction((Transaction myTransaction) async {
      await myTransaction.delete(eventRef);
    });
  } catch (e) {
    print("error");
  }
}

Future<void> leaveEvent(String id) async {
  Firestore _firestore = Firestore.instance;
  DocumentReference eventRef = _firestore.collection('events').document(id);
  DocumentSnapshot d1 = await eventRef.get();
  var keys = d1.data['attendees'].keys.toList();
  DocumentReference userRef = _firestore.collection('users').document(fbu.uid);
  await userRef.updateData({'pastEventsID.$id': FieldValue.delete()});
  await eventRef.updateData({"attendees.${fbu.uid}": FieldValue.delete()});
}

Future<List<int>> getMutualFriends3(List<User> friends) async {
  List<int> sharedInts = [];
  Firestore _firestore = Firestore.instance;
  for (int a = 0; a < friends.length; a++) {
    DocumentReference userRef =
        _firestore.collection('users').document(friends[a].userID);
    DocumentSnapshot d1 = await userRef.get();
    var data = d1.data['friends'].keys.toList();
    DocumentReference userRef2 =
        _firestore.collection('users').document(fbu.uid);
    DocumentSnapshot d2 = await userRef2.get();
    var data2 = d2.data['friends'].keys.toList();
    int shared = 0;
    for (int i = 0; i < data.length; i++) {
      for (int j = 0; j < data2.length; j++) {
        if (data[i] == data2[j]) {
          shared++;
        }
      }
    }
    sharedInts.add(shared);
  }
  return sharedInts;
}

Future<List<User>> getMutualFriends2(List<User> friends) async {
  List<User> friendsSorted = [];
  Map<int, int> map = new Map<int, int>();
  for (int i = 0; i < friends.length; i++) {
    int a = await getMutualFriends(friends[i].userID, fbu.uid);
    map[i] = a;
  }
  var sortedMap = Map.fromEntries(
      map.entries.toList()..sort((e1, e2) => e1.value.compareTo(e2.value)));
  var keys = sortedMap.keys.toList();
  for (int i = sortedMap.keys.length - 1; i >= 0; i--) {
    friendsSorted.add(friends[keys[i]]);
  }
  return friendsSorted;
}

Future<int> getMutualFriends(String userID, String userID2) async {
  Firestore _firestore = Firestore.instance;
  DocumentReference userRef = _firestore.collection('users').document(userID);
  DocumentSnapshot d1 = await userRef.get();
  var data = d1.data['friends'].keys.toList();
  DocumentReference userRef2 = _firestore.collection('users').document(userID2);
  DocumentSnapshot d2 = await userRef2.get();
  var data2 = d2.data['friends'].keys.toList();
  int shared = 0;
  for (int i = 0; i < data.length; i++) {
    for (int j = 0; j < data2.length; j++) {
      if (data[i] == data2[j]) {
        shared++;
      }
    }
  }
  return shared;
}

Future<List<Event>> getEvents() async {
  var fs = Firestore.instance;
  List<Event> events = [];
  DocumentReference userRef = fs.collection('users').document(fbu.uid);
  var d1 = await userRef.get();
  var data = d1.data['eventsID'];
  for (String eventId in data.keys) {
    DocumentReference eventRef = fs.collection('events').document(eventId);
    var eventSnapshot = await eventRef.get();
    String start = eventSnapshot.data['start'];
    String end = eventSnapshot.data['end'];
    var now = new DateTime.now();
    if (now.compareTo(DateTime.parse(end)) < 0) {
      String name = eventSnapshot.data['name'];
      String id = eventSnapshot.data['id'];
      String hostName = "";
      DocumentSnapshot snapHost = await fs
          .collection('users')
          .document(eventSnapshot.data['host.id'])
          .get();
      hostName = snapHost.data['username'];
      // String eventImage = eventSnapshot.data['image'];
      String location = eventSnapshot.data['location'];
      String messagesID = eventSnapshot.data['messagesID'];
      StorageReference imageRef = FirebaseStorage.instance
          .ref()
          .child('profiles/${eventSnapshot.data['host.id']}');
      String url = await imageRef.getDownloadURL();
      ImageProvider<dynamic> image = NetworkImage(url);
      AttendanceState attendanceState =
          toAttendanceState(d1.data['eventsID'][eventId]);
      Map<User, AttendanceState> attendees = {};
      var attendeeData = eventSnapshot.data['attendees'];
      for (var v in attendeeData.keys.toList()) {
        DocumentSnapshot attD = await fs.collection('users').document(v).get();
        AttendanceState att = toAttendanceState(attD.data['eventsID'][eventId]);
        DocumentSnapshot attendeeUserSnapshot =
            await fs.collection('users').document(v).get();
        String attendeeName = attendeeUserSnapshot.data['username'];
        StorageReference imageRef =
            FirebaseStorage.instance.ref().child('profiles/$v');
        String url = await imageRef.getDownloadURL();
        ImageProvider<dynamic> image = NetworkImage(url);
        User invitedUser = new User(
            userName: attendeeName,
            userImage: image,
            userID: attendeeUserSnapshot.data['user_id']);
        attendees[invitedUser] = att;
      }
      Event e = new Event(
          messagesID: messagesID,
          id: id,
          title: name,
          start: DateTime.parse(start),
          end: DateTime.parse(end),
          host: User(
              userName: hostName,
              userImage: image,
              userID: eventSnapshot.data['host.id']),
          hostID: eventSnapshot.data['host.id'],
          attendees: attendees,
          attendanceState: attendanceState,
          location: location);
      List<int> coded = utf8.encode(e.id);
      var sum = coded.reduce((a, b) => a + b);
      await notif.notify(
          e.start.subtract(Duration(days: 1)),
          "Don't forget about ${e.title} tomorrow!",
          "It will start ${DateFormat('hh:mm a').format(e.start)} tomorrow at ${e.location}",
          sum);
      await notif.notify(
          e.start.subtract(Duration(minutes: 60)),
          "${e.title} is happening at ${DateFormat('hh:mm a').format(e.start)} today!",
          "Meet at ${e.location} in 1 hour",
          sum + 1);
      await notif.notify(e.start, "${e.title} has started!",
          "It is happening at ${e.location}", sum + 2);
      events.add(e);
    } else {
      var attendeeData = eventSnapshot.data['attendees'];
      for (var v in attendeeData.keys.toList()) {
        DocumentReference userRef2 = fs.collection('users').document(v);
        DocumentSnapshot attD = await userRef2.get();
        String att = attD.data['eventsID'][eventId];
        print(att);
        await userRef2.updateData({"pastEventsID.${eventSnapshot.data['id']}": att});
        await userRef2.updateData(
            {"eventsID.${eventSnapshot.data['id']}": FieldValue.delete()});
      }
    }
    await notif.pending();
  }
  return sortEvents(events, false);
}

List<Event> sortEvents(List<Event> events, bool isPast) {
  List<Event> eventsSorted = [];
  Map<int, DateTime> map = new Map<int, DateTime>();
  for (int i = 0; i < events.length; i++) {
    map[i] = events[i].start;
  }

  var sortedMap = Map.fromEntries(
      map.entries.toList()..sort((e1, e2) => e1.value.compareTo(e2.value)));
  var keys = sortedMap.keys.toList();
  if (!isPast) {
    for (int i = 0; i < keys.length; i++) {
      eventsSorted.add(events[keys[i]]);
    }
  } else {
    for (int i = keys.length - 1; i >= 0; i--) {
      eventsSorted.add(events[keys[i]]);
    }
  }
  return eventsSorted;
}

Future<List<Event>> getPastEvents() async {
  Firestore _firestore = Firestore.instance;
  List<Event> events = [];
  DocumentReference userRef = _firestore.collection('users').document(fbu.uid);
  DocumentSnapshot d1 = await userRef.get();
  var data = d1.data['pastEventsID'];
  for (int a = 0; a < data.keys.length; a++) {
    var keys0 = data.keys.toList();
    DocumentReference eventRef =
        _firestore.collection('events').document(keys0[a]);
    await eventRef.get().then((datasnapshot2) async {
      DocumentSnapshot d2 = datasnapshot2;
      var now = new DateTime.now();
      String start = d2.data['start'];
      String end = d2.data['end'];
      if (now.compareTo(DateTime.parse(end)) > 0) {
        String name = d2.data['name'];
        String id = d2.data['id'];
        String hostName = "";
        DocumentReference hostRef =
            _firestore.collection('users').document(d2.data['host.id']);
        DocumentSnapshot snapHost = await hostRef.get();
        hostName = snapHost.data['username'];
        String eventImage = d2.data['image'];
        String location = d2.data['location'];
        String messagesID = d2.data['messagesID'];
        StorageReference imageRef = FirebaseStorage.instance
            .ref()
            .child('profiles/${d2.data['host.id']}');
        String url = await imageRef.getDownloadURL();
        ImageProvider<dynamic> image = NetworkImage(url);
        AttendanceState attendanceState =
            toAttendanceState(d1.data['pastEventsID'][keys0[a]]);
        Map<User, AttendanceState> attendees = {};
        var data2 = d2.data['attendees'];
        for (int i = 0; i < data2.keys.length; i++) {
          var keys = data2.keys.toList();
          var v = keys[i];
          DocumentReference attRef = _firestore.collection('users').document(v);
          DocumentSnapshot attD = await attRef.get();
          AttendanceState att =
              toAttendanceState(attD.data['pastEventsID'][keys0[a]]);
          DocumentReference attendeeRef =
              _firestore.collection('users').document(v);
          DocumentSnapshot datasnapshot3 = await attendeeRef.get();
          String attendeeName = datasnapshot3.data['username'];
          StorageReference imageRef =
              FirebaseStorage.instance.ref().child('profiles/$v');
          String url = await imageRef.getDownloadURL();
          ImageProvider<dynamic> image = NetworkImage(url);
          User invitedUser = new User(
              userName: attendeeName,
              userImage: image,
              userID: datasnapshot3.data['user_id']);
          attendees[invitedUser] = att;
        }
        Event e = new Event(
            messagesID: messagesID,
            id: id,
            title: name,
            start: DateTime.parse(start),
            end: DateTime.parse(end),
            host: User(
                userName: hostName,
                userImage: image,
                userID: d2.data['host.id']),
            hostID: d2.data['host.id'],
            attendees: attendees,
            attendanceState: attendanceState,
            location: location);
        events.add(e);
      }
    });
  }
  return sortEvents(events, true);
}
