import "package:flutter/material.dart";
import 'package:grouper/colors.dart';
import 'components.dart';

const DEFAULT_FONT_FAMILY = "Quicksand";
const CHAT_FONT_FAMILY = "Roboto";

final big = TextStyle(
    fontFamily: DEFAULT_FONT_FAMILY,
    fontWeight: FontWeight.w500,
    fontSize: 20 * sr,
    height: (24 / 20) * hr);
final medium = TextStyle(
  fontFamily: DEFAULT_FONT_FAMILY,
  fontWeight: FontWeight.w500,
  fontSize: 16 * sr,
  height: 1 * hr,
);
final mediumUnderlined = TextStyle(
    fontFamily: DEFAULT_FONT_FAMILY,
    fontWeight: FontWeight.w500,
    fontSize: 18 * sr,
    height: 1 * hr,
    decoration: TextDecoration.underline);
final small = TextStyle(
  fontFamily: DEFAULT_FONT_FAMILY,
  fontWeight: FontWeight.w500,
  fontSize: 16 * sr,
);
final extraSmall = TextStyle(
  fontFamily: DEFAULT_FONT_FAMILY,
  fontWeight: FontWeight.w500,
  fontSize: 10 * sr,
);
final header = TextStyle(
  fontFamily: DEFAULT_FONT_FAMILY,
  fontWeight: FontWeight.w500,
  fontSize: 28 * sr,
);

// Text for Settings Elements
final smallRed = TextStyle(
    fontFamily: DEFAULT_FONT_FAMILY,
    fontWeight: FontWeight.w500,
    fontSize: 16 * sr,
    color: SIGN_OUT);

TextStyle withColor(Color c, TextStyle style) =>
    style.merge(TextStyle(color: c));
